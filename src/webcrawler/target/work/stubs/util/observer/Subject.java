package util.observer;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

public interface Subject
 {
;
 void register(util.observer.Observer obj);
 void unregister(util.observer.Observer obj);
 void notifyObservers();
 java.lang.Object getUpdate(util.observer.Observer obj);
}
