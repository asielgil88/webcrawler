package util.mapper;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

public class BiddingMapper
  extends util.mapper.BaseMapper<retrofit.response.lista.dependencias.BiddingV2Response, model.v2.bidding.Bidding> {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public static  int getHEADER_NUM_POS() { return (int)0;}
public static  int getHEADER_DESCRIPTION_POS() { return (int)0;}
public static  int getHEADER_OBJ_POS() { return (int)0;}
public static  int getENTIDAD_NAME_POS() { return (int)0;}
public static  int getENTIDAD_DEPENDENCIA_POS() { return (int)0;}
public static  int getENTIDAD_UNIDAD_POS() { return (int)0;}
public static  int getENTIDAD_DIRECCION_POS() { return (int)0;}
public static  int getCONTACT_NOMBRE_POS() { return (int)0;}
public static  int getCONTACT_CARGO_POS() { return (int)0;}
@java.lang.Override() public  model.v2.bidding.Bidding transform(retrofit.response.lista.dependencias.BiddingV2Response input) { return (model.v2.bidding.Bidding)null;}
}
