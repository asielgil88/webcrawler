package util.mapper;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

public abstract class BaseMapper
<I, O>  extends java.lang.Object  implements
    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  void baseTransform(O output) { }
public abstract  O transform(I input);
public  java.util.ArrayList<O> transform(java.util.ArrayList<I> collections) { return (java.util.ArrayList<O>)null;}
public  java.lang.Object findValueByName(java.util.List<retrofit.response.lista.dependencias.LabelV2Response> values, int pos) { return null;}
}
