package model;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

public class SysUserRole
  extends java.lang.Object  implements
    java.io.Serializable,    groovy.lang.GroovyObject {
;
public static  model.SysUserRole create(model.SysUser sysUser, model.Role role) { return (model.SysUserRole)null;}
public static  boolean remove(model.SysUser u, model.Role r) { return false;}
public static  void removeAll(model.SysUser u) { }
public static  void removeAll(model.Role r) { }
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  model.SysUser getSysUser() { return (model.SysUser)null;}
public  void setSysUser(model.SysUser value) { }
public  model.Role getRole() { return (model.Role)null;}
public  void setRole(model.Role value) { }
public static  java.lang.Object getConstraints() { return null;}
public static  void setConstraints(java.lang.Object value) { }
public static  java.lang.Object getMapping() { return null;}
public static  void setMapping(java.lang.Object value) { }
public  boolean equals(java.lang.Object other) { return false;}
public  int hashCode() { return (int)0;}
public static  model.SysUserRole get(long sysUserId, long roleId) { return (model.SysUserRole)null;}
public static  boolean exists(long sysUserId, long roleId) { return false;}
public static  model.SysUserRole create(model.SysUser sysUser, model.Role role, boolean flush) { return (model.SysUserRole)null;}
public static  boolean remove(model.SysUser u, model.Role r, boolean flush) { return false;}
public static  void removeAll(model.SysUser u, boolean flush) { }
public static  void removeAll(model.Role r, boolean flush) { }
}
