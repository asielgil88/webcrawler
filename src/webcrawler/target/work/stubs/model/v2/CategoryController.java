package model.v2;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

public class CategoryController
  extends java.lang.Object  implements
    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  java.lang.Object getScrapService() { return null;}
public  void setScrapService(java.lang.Object value) { }
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN","ROLE_USER"}) public  java.lang.Object index(java.lang.Integer max) { return null;}
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN"}) public  java.lang.Object categoryRefresh() { return null;}
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN"}) @grails.transaction.Transactional() public  java.lang.Object enableCategoria(int id) { return null;}
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN"}) @grails.transaction.Transactional() public  java.lang.Object disableCategoria(int id) { return null;}
}
