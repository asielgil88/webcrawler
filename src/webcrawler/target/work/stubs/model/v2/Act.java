package model.v2;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

public class Act
  extends java.lang.Object  implements
    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  java.util.Date getDateCreated() { return (java.util.Date)null;}
public  void setDateCreated(java.util.Date value) { }
public  java.util.Date getLastUpdated() { return (java.util.Date)null;}
public  void setLastUpdated(java.util.Date value) { }
public  java.lang.String getNumeroAdquisicion() { return (java.lang.String)null;}
public  void setNumeroAdquisicion(java.lang.String value) { }
public  java.lang.String getDescripcionAdquisicion() { return (java.lang.String)null;}
public  void setDescripcionAdquisicion(java.lang.String value) { }
public  java.lang.String getNombreUnidadCompra() { return (java.lang.String)null;}
public  void setNombreUnidadCompra(java.lang.String value) { }
public  java.lang.String getNombreDependencia() { return (java.lang.String)null;}
public  void setNombreDependencia(java.lang.String value) { }
public  java.lang.String getEstado() { return (java.lang.String)null;}
public  void setEstado(java.lang.String value) { }
public  int getNnc() { return (int)0;}
public  void setNnc(int value) { }
public  java.lang.String getIdEmpresa() { return (java.lang.String)null;}
public  void setIdEmpresa(java.lang.String value) { }
public  java.lang.String getModalidad() { return (java.lang.String)null;}
public  void setModalidad(java.lang.String value) { }
public  float getMontoRef() { return (float)0;}
public  void setMontoRef(float value) { }
public  java.lang.String getFecha() { return (java.lang.String)null;}
public  void setFecha(java.lang.String value) { }
public  boolean getProcessed() { return false;}
public  boolean isProcessed() { return false;}
public  void setProcessed(boolean value) { }
public  model.v2.Category getCategory() { return (model.v2.Category)null;}
public  void setCategory(model.v2.Category value) { }
public  model.v2.OtherContract getOtherContract() { return (model.v2.OtherContract)null;}
public  void setOtherContract(model.v2.OtherContract value) { }
public static  java.lang.Object getConstraints() { return null;}
public static  void setConstraints(java.lang.Object value) { }
public static  java.lang.Object getMapping() { return null;}
public static  void setMapping(java.lang.Object value) { }
}
