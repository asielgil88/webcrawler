package model.v2;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

public class Category
  extends java.lang.Object  implements
    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  java.lang.String getNivel1() { return (java.lang.String)null;}
public  void setNivel1(java.lang.String value) { }
public  java.lang.String getIdRubro() { return (java.lang.String)null;}
public  void setIdRubro(java.lang.String value) { }
public  java.lang.String getPrimerNivel() { return (java.lang.String)null;}
public  void setPrimerNivel(java.lang.String value) { }
public  java.lang.String getTotal() { return (java.lang.String)null;}
public  void setTotal(java.lang.String value) { }
public  java.util.Date getLastUpdated() { return (java.util.Date)null;}
public  void setLastUpdated(java.util.Date value) { }
public  boolean getEnabled() { return false;}
public  boolean isEnabled() { return false;}
public  void setEnabled(boolean value) { }
public static  java.lang.Object getConstraints() { return null;}
public static  void setConstraints(java.lang.Object value) { }
public  java.lang.Object updateModel(model.v2.Category item) { return null;}
public  int countBidToday() { return (int)0;}
public  int countBidTotal() { return (int)0;}
}
