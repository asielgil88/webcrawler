package model.v2.bidding;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

@groovy.transform.ToString(includeNames=true, excludes="id, version, dateCreated, myMailProviderService, category, observada, lastUpdated, especificacionesTecnicas", includeFields=true) public class Bidding
  extends java.lang.Object  implements
    util.domain.DomainInterface<model.v2.bidding.Bidding>,    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  java.lang.Object getMyMailProviderService() { return null;}
public  void setMyMailProviderService(java.lang.Object value) { }
public  java.util.Date getDateCreated() { return (java.util.Date)null;}
public  void setDateCreated(java.util.Date value) { }
public  java.util.Date getLastUpdated() { return (java.util.Date)null;}
public  void setLastUpdated(java.util.Date value) { }
public  model.v2.Category getCategory() { return (model.v2.Category)null;}
public  void setCategory(model.v2.Category value) { }
public  model.v2.OtherContract getOtherContract() { return (model.v2.OtherContract)null;}
public  void setOtherContract(model.v2.OtherContract value) { }
public  boolean getObservada() { return false;}
public  boolean isObservada() { return false;}
public  void setObservada(boolean value) { }
public  java.lang.String getNumero() { return (java.lang.String)null;}
public  void setNumero(java.lang.String value) { }
public  java.lang.String getDescripcion() { return (java.lang.String)null;}
public  void setDescripcion(java.lang.String value) { }
public  java.lang.String getObjetoContratacion() { return (java.lang.String)null;}
public  void setObjetoContratacion(java.lang.String value) { }
public  java.lang.String getEntidad() { return (java.lang.String)null;}
public  void setEntidad(java.lang.String value) { }
public  java.lang.String getDependencias() { return (java.lang.String)null;}
public  void setDependencias(java.lang.String value) { }
public  java.lang.String getUnidadDeCompra() { return (java.lang.String)null;}
public  void setUnidadDeCompra(java.lang.String value) { }
public  java.lang.String getDireccion() { return (java.lang.String)null;}
public  void setDireccion(java.lang.String value) { }
public  java.lang.String getContactUserNombre() { return (java.lang.String)null;}
public  void setContactUserNombre(java.lang.String value) { }
public  java.lang.String getContactUserCargo() { return (java.lang.String)null;}
public  void setContactUserCargo(java.lang.String value) { }
public  model.v2.bidding.TechSpec getEspecificacionesTecnicas() { return (model.v2.bidding.TechSpec)null;}
public  void setEspecificacionesTecnicas(model.v2.bidding.TechSpec value) { }
public static  java.lang.Object getHasMany() { return null;}
public static  void setHasMany(java.lang.Object value) { }
public static  java.lang.Object getConstraints() { return null;}
public static  void setConstraints(java.lang.Object value) { }
public static  java.lang.Object getMapping() { return null;}
public static  void setMapping(java.lang.Object value) { }
public  boolean isModelChanged(model.v2.bidding.Bidding source) { return false;}
public  void saveProcedure() { }
public  void saveValidation() { }
public  boolean compare(model.v2.bidding.Bidding other) { return false;}
public  java.lang.Object afterInsert() { return null;}
public  java.lang.Object getPanamaLink() { return null;}
public  java.lang.String getString() { return (java.lang.String)null;}
public  java.lang.Object getFechaPublicacion() { return null;}
public  java.lang.Object getFechaRecepcion() { return null;}
public  java.lang.Object getFechaPresentacion() { return null;}
public  java.lang.Object cleanAllStrings() { return null;}
}
