package model.v2.bidding;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

@groovy.transform.ToString(includeNames=true, excludes="id,version,techSpec", includeFields=true) public class TechSpecItemLevel2
  extends java.lang.Object  implements
    util.domain.DomainInterface<model.v2.bidding.TechSpecItemLevel2>,    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  int getNumeroLinea() { return (int)0;}
public  void setNumeroLinea(int value) { }
public  long getCodigo() { return (long)0;}
public  void setCodigo(long value) { }
public  java.lang.String getDescripcion() { return (java.lang.String)null;}
public  void setDescripcion(java.lang.String value) { }
public  java.lang.String getCantidad() { return (java.lang.String)null;}
public  void setCantidad(java.lang.String value) { }
public  java.lang.String getMedida() { return (java.lang.String)null;}
public  void setMedida(java.lang.String value) { }
public  java.lang.String getGlosa() { return (java.lang.String)null;}
public  void setGlosa(java.lang.String value) { }
public  java.lang.String getCodigoSes() { return (java.lang.String)null;}
public  void setCodigoSes(java.lang.String value) { }
public static  java.lang.Object getBelongsTo() { return null;}
public static  void setBelongsTo(java.lang.Object value) { }
public static  java.lang.Object getConstraints() { return null;}
public static  void setConstraints(java.lang.Object value) { }
public  boolean compare(model.v2.bidding.TechSpecItemLevel2 other) { return false;}
public  java.lang.String getString() { return (java.lang.String)null;}
}
