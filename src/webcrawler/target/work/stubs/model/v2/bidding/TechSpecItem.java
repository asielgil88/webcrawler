package model.v2.bidding;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

@groovy.transform.ToString(includeNames=true, excludes="id,version,techSpec", includeFields=true) public class TechSpecItem
  extends java.lang.Object  implements
    util.domain.DomainInterface<model.v2.bidding.TechSpecItem>,    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  java.lang.String getTitle() { return (java.lang.String)null;}
public  void setTitle(java.lang.String value) { }
public  java.lang.String getTypeValue() { return (java.lang.String)null;}
public  void setTypeValue(java.lang.String value) { }
public static  java.lang.Object getBelongsTo() { return null;}
public static  void setBelongsTo(java.lang.Object value) { }
public static  java.lang.Object getHasMany() { return null;}
public static  void setHasMany(java.lang.Object value) { }
public static  java.lang.Object getMapping() { return null;}
public static  void setMapping(java.lang.Object value) { }
public static  java.lang.Object getConstraints() { return null;}
public static  void setConstraints(java.lang.Object value) { }
public  boolean compare(model.v2.bidding.TechSpecItem other) { return false;}
public  java.lang.String getString() { return (java.lang.String)null;}
}
