package model.v2.bidding;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

@groovy.transform.ToString(includeNames=true, excludes="id,version,link", includeFields=true) public class DocAttached
  extends java.lang.Object  implements
    util.domain.DomainInterface<model.v2.bidding.DocAttached>,    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  int getTipoFiltro() { return (int)0;}
public  void setTipoFiltro(int value) { }
public  java.lang.String getLink() { return (java.lang.String)null;}
public  void setLink(java.lang.String value) { }
public  java.lang.String getComentario() { return (java.lang.String)null;}
public  void setComentario(java.lang.String value) { }
public  java.lang.String getDescription() { return (java.lang.String)null;}
public  void setDescription(java.lang.String value) { }
public  java.lang.String getFechaCreacion() { return (java.lang.String)null;}
public  void setFechaCreacion(java.lang.String value) { }
public static  java.lang.Object getConstraints() { return null;}
public static  void setConstraints(java.lang.Object value) { }
public static  java.lang.Object getMapping() { return null;}
public static  void setMapping(java.lang.Object value) { }
public  boolean compare(model.v2.bidding.DocAttached other) { return false;}
public  java.lang.String getString() { return (java.lang.String)null;}
public  java.lang.Object getPanamaLink() { return null;}
}
