package model.v2;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@grails.transaction.Transactional(readOnly=true) public class BiddingController
  extends java.lang.Object  implements
    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  java.lang.Object getScrapService() { return null;}
public  void setScrapService(java.lang.Object value) { }
public  java.lang.Object getSpringSecurityService() { return null;}
public  void setSpringSecurityService(java.lang.Object value) { }
public  java.lang.Object getMyMailProviderService() { return null;}
public  void setMyMailProviderService(java.lang.Object value) { }
public static  java.lang.Object getAllowedMethods() { return null;}
public static  void setAllowedMethods(java.lang.Object value) { }
@grails.plugin.springsecurity.annotation.Secured(value="permitAll") public  java.lang.Object showPublic(model.v2.bidding.Bidding biddingV2Instance) { return null;}
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN","ROLE_USER"}) public  java.lang.Object index(java.lang.Integer max) { return null;}
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN","ROLE_USER"}) public  java.lang.Object getByOtherContract(java.lang.Integer max) { return null;}
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN","ROLE_USER"}) public  java.lang.Object show(model.v2.bidding.Bidding biddingV2Instance) { return null;}
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN","ROLE_USER"}) public  java.lang.Object search(java.lang.Integer max, java.lang.Integer offset, java.lang.String q) { return null;}
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN","ROLE_USER"}) public  java.lang.Object watchlist(java.lang.Integer max, java.lang.Integer offset) { return null;}
@grails.plugin.springsecurity.annotation.Secured(value="ROLE_ADMIN") @grails.transaction.Transactional() public  java.lang.Object migration1() { return null;}
@grails.plugin.springsecurity.annotation.Secured(value="ROLE_ADMIN") @grails.transaction.Transactional() public  java.lang.Object migration4() { return null;}
@grails.plugin.springsecurity.annotation.Secured(value="ROLE_ADMIN") @grails.transaction.Transactional() public  java.lang.Object testEmail() { return null;}
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN","ROLE_USER"}) @grails.transaction.Transactional() public  java.lang.Object toggleWatch(java.lang.Integer id) { return null;}
@grails.plugin.springsecurity.annotation.Secured(value={"ROLE_ADMIN","ROLE_USER"}) protected  void notFound() { }
}
