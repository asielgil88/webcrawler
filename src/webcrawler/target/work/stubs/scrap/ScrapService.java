package scrap;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

@grails.transaction.Transactional() public class ScrapService
  extends java.lang.Object  implements
    groovy.lang.GroovyObject {
;
public static final int INICIO_LIST_ACTOS = (int) 0;
public static final java.lang.String INICIO_LIST_OTHERS = null;
public static final java.lang.String LIST_ACTOS_METHOD = null;
public static java.lang.String LIST_ACTOS_VALUE;
public static final java.lang.String LISTAR_DEPENDENCIAS_METHOD = null;
public static java.lang.String LISTAR_DEPENDENCIAS_VALUE;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public  java.lang.Object getMyMailProviderService() { return null;}
public  void setMyMailProviderService(java.lang.Object value) { }
public  java.lang.Object fetchCategories() { return null;}
public  java.lang.Object fetchListActos(model.v2.Category categoryV2, int inicio) { return null;}
public  java.lang.Object fetchOthersListActos(model.v2.OtherContract otherContract) { return null;}
public  java.lang.Object processOtherContracts(model.v2.OtherContract otherContract) { return null;}
public  java.lang.Object processActs(model.v2.Category category) { return null;}
public  java.lang.Object fetchListDependencias(model.v2.Category category, java.lang.String numeroAdquisicion) { return null;}
public  java.lang.Object fetchListDependenciasByContract(model.v2.OtherContract contract, java.lang.String numeroAdquisicion) { return null;}
public  java.lang.Object migrateWatchlist() { return null;}
public  java.lang.Object migrateAlert() { return null;}
public  java.lang.Object migrateCleanWatchlist() { return null;}
}
