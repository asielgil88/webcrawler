package mail;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

@grails.transaction.Transactional() public class MyMailProviderService
  extends java.lang.Object  implements
    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public final  java.lang.String getCOLOR_ALERT() { return (java.lang.String)null;}
public final  java.lang.String getCOLOR_WATCH_ALERT() { return (java.lang.String)null;}
public  java.lang.Object getMailService() { return null;}
public  void setMailService(java.lang.Object value) { }
public  grails.gsp.PageRenderer getGroovyPageRenderer() { return (grails.gsp.PageRenderer)null;}
public  void setGroovyPageRenderer(grails.gsp.PageRenderer value) { }
public  org.codehaus.groovy.grails.web.mapping.LinkGenerator getGrailsLinkGenerator() { return (org.codehaus.groovy.grails.web.mapping.LinkGenerator)null;}
public  void setGrailsLinkGenerator(org.codehaus.groovy.grails.web.mapping.LinkGenerator value) { }
public  java.lang.Object insertAlert(model.v2.bidding.Bidding bidding, java.lang.String alert) { return null;}
public  java.lang.Object watchAlert(model.v2.bidding.Bidding bidding) { return null;}
public  java.lang.Object watchAlertAdmin(model.v2.bidding.Bidding bidding) { return null;}
public  java.lang.Object reportException(java.lang.Exception ex, java.lang.Object param) { return null;}
public  java.lang.Object reportEvent(java.lang.String event) { return null;}
public  java.lang.Object sendEmailTest() { return null;}
@java.lang.Deprecated() public  java.lang.Object watchListAlert(java.util.List<model.v2.bidding.Bidding> biddings) { return null;}
}
