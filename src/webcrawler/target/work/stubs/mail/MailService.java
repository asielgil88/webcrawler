package mail;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import groovy.lang.*;
import groovy.util.*;

public class MailService
  extends java.lang.Object  implements
    org.springframework.beans.factory.InitializingBean,    org.springframework.beans.factory.DisposableBean,    groovy.lang.GroovyObject {
;
public  groovy.lang.MetaClass getMetaClass() { return (groovy.lang.MetaClass)null;}
public  void setMetaClass(groovy.lang.MetaClass mc) { }
public  java.lang.Object invokeMethod(java.lang.String method, java.lang.Object arguments) { return null;}
public  java.lang.Object getProperty(java.lang.String property) { return null;}
public  void setProperty(java.lang.String property, java.lang.Object value) { }
public static  java.lang.Object getTransactional() { return null;}
public static  void setTransactional(java.lang.Object value) { }
public  org.codehaus.groovy.grails.commons.GrailsApplication getGrailsApplication() { return (org.codehaus.groovy.grails.commons.GrailsApplication)null;}
public  void setGrailsApplication(org.codehaus.groovy.grails.commons.GrailsApplication value) { }
public  grails.plugin.mail.MailMessageBuilderFactory getMailMessageBuilderFactory() { return (grails.plugin.mail.MailMessageBuilderFactory)null;}
public  void setMailMessageBuilderFactory(grails.plugin.mail.MailMessageBuilderFactory value) { }
public  java.util.concurrent.ThreadPoolExecutor getMailExecutorService() { return (java.util.concurrent.ThreadPoolExecutor)null;}
public  void setMailExecutorService(java.util.concurrent.ThreadPoolExecutor value) { }
public  org.springframework.mail.MailMessage sendMail(java.lang.Object config, groovy.lang.Closure callable) { return (org.springframework.mail.MailMessage)null;}
public  org.springframework.mail.MailMessage sendMail(groovy.lang.Closure callable) { return (org.springframework.mail.MailMessage)null;}
public  groovy.util.ConfigObject getMailConfig() { return (groovy.util.ConfigObject)null;}
public  boolean isDisabled() { return false;}
public  void setPoolSize(java.lang.Integer poolSize) { }
@java.lang.Override() public  void destroy()throws java.lang.Exception { }
@java.lang.Override() public  void afterPropertiesSet()throws java.lang.Exception { }
}
