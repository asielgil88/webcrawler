package util.mapper

import model.v2.bidding.Label
import retrofit.response.lista.dependencias.LabelV2Response

class LabelMapper extends BaseMapper<LabelV2Response, Label>{


    @Override
    Label transform(LabelV2Response input) {

        if(input == null){
            return null
        }

        Label output = new Label()
        output.label = input.label
        output.value_field = input.value ? input.value : ""

        baseTransform(output)

        return output
    }

}
