package util.mapper

import model.v2.bidding.TechSpecItem
import model.v2.bidding.TechSpec
import retrofit.response.lista.dependencias.TechSpecItemV2Response

class TechSpecItemMapper extends BaseMapper<TechSpecItemV2Response, TechSpecItem> {


    TechSpec techSpec

    TechSpecItemMapper(TechSpec techSpecV2) {
        this.techSpec = techSpecV2
    }

    @Override
    TechSpecItem transform(TechSpecItemV2Response input) {

        if (input == null) {
            return null
        }

        TechSpecItem output = new TechSpecItem()

        TechSpecItemLevel2Mapper mapper = new TechSpecItemLevel2Mapper(output)

        output.techSpec = techSpec
        output.title = input.title
        output.typeValue = input.typeValue

        output.values = mapper.transform(input.values)

        baseTransform(output)

        return output
    }
}
