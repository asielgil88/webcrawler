package util.mapper

import model.v2.bidding.Bidding
import retrofit.response.lista.dependencias.BiddingV2Response

class BiddingMapper extends BaseMapper<BiddingV2Response, Bidding> {

    static final int HEADER_NUM_POS = 0
    static final int HEADER_DESCRIPTION_POS = 1
    static final int HEADER_OBJ_POS = 2

    static final int ENTIDAD_NAME_POS = 0
    static final int ENTIDAD_DEPENDENCIA_POS = 1
    static final int ENTIDAD_UNIDAD_POS = 2
    static final int ENTIDAD_DIRECCION_POS = 3

    static final int CONTACT_NOMBRE_POS = 0
    static final int CONTACT_CARGO_POS = 1


    @Override
    Bidding transform(BiddingV2Response input) {

        if (input == null) {
            return null
        }

        Bidding output = new Bidding()

        TechSpecMapper techSpecMapper = new TechSpecMapper(output)
        DocAttachedMapper docAttachedMapper = new DocAttachedMapper()
        LabelMapper labelMapper = new LabelMapper()

        output.numero = findValueByName(input.header.values, HEADER_NUM_POS)
        output.descripcion = findValueByName(input.header.values, HEADER_DESCRIPTION_POS)
        output.objetoContratacion = findValueByName(input.header.values, HEADER_OBJ_POS)
        output.entidad = findValueByName(input.entityInformation.values, ENTIDAD_NAME_POS)
        output.dependencias = findValueByName(input.entityInformation.values, ENTIDAD_DEPENDENCIA_POS)
        output.unidadDeCompra = findValueByName(input.entityInformation.values, ENTIDAD_UNIDAD_POS)
        output.direccion = findValueByName(input.entityInformation.values, ENTIDAD_DIRECCION_POS)
        output.contactUserNombre = findValueByName(input.contactUser.values, CONTACT_NOMBRE_POS)
        output.contactUserCargo = findValueByName(input.contactUser.values, CONTACT_CARGO_POS)

        output.especificacionesTecnicas = techSpecMapper.transform(input.especificacionesTecnicas)
        output.documentosAdjuntos = docAttachedMapper.transform(input.documentosAdjuntos.values)
        output.avisos = labelMapper.transform(input.aviso.values)

        baseTransform(output)

        return output
    }

}
