package util.mapper

import model.v2.bidding.Bidding
import model.v2.bidding.TechSpec
import retrofit.response.lista.dependencias.TechSpecV2Response

class TechSpecMapper extends BaseMapper<TechSpecV2Response, TechSpec> {

    Bidding biddingV2

    TechSpecMapper(Bidding biddingV2) {
        this.biddingV2 = biddingV2
    }

    @Override
    TechSpec transform(TechSpecV2Response input) {

        if (input == null) {
            return null
        }

        TechSpec output = new TechSpec()

        TechSpecItemMapper techSpecItemMapper = new TechSpecItemMapper(output)

        output.title = input.title
        output.values = techSpecItemMapper.transform(input.values)

        baseTransform(output)

        return output
    }
}
