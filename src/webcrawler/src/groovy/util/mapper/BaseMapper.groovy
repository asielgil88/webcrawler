package util.mapper

import retrofit.response.lista.dependencias.LabelV2Response
import util.ClassUtils

abstract class BaseMapper<I, O> {

    void baseTransform(O output){
        ClassUtils.cleanStringFields(output)
    }

    abstract O transform(I input)

    ArrayList<O> transform(ArrayList<I> collections) {

        ArrayList<O> results = new ArrayList<>()
        collections.each {
            results << transform(it)
        }
        return results
    }

    def findValueByName(List<LabelV2Response> values, int pos) {
        if (values.size() > pos) {
            return values.get(pos).value
        }
        return null
    }

}
