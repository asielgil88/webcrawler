package util.mapper

import model.v2.bidding.DocAttached
import retrofit.response.lista.dependencias.DocAttachedV2Response

class DocAttachedMapper extends BaseMapper<DocAttachedV2Response, DocAttached>{

    @Override
    DocAttached transform(DocAttachedV2Response input) {

        if (input == null) {
            return null
        }

        DocAttached output = new DocAttached()
        output.tipoFiltro = input.tipoFiltro
        output.link = input.link
        output.comentario = input.comentario
        output.description = input.descripcion
        output.fechaCreacion = input.fechaCreacion

        baseTransform(output)

        return output
    }

}
