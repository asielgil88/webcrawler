package util.mapper

import model.v2.bidding.TechSpecItemLevel2
import model.v2.bidding.TechSpecItem
import retrofit.response.lista.dependencias.TechSpecItemLevel2Response

class TechSpecItemLevel2Mapper extends BaseMapper<TechSpecItemLevel2Response, TechSpecItemLevel2> {

    TechSpecItem techSpecItemV2

    TechSpecItemLevel2Mapper(TechSpecItem techSpecItemV2){
        this.techSpecItemV2 = techSpecItemV2
    }

    @Override
    TechSpecItemLevel2 transform(TechSpecItemLevel2Response input) {

        if (input == null) {
            return null
        }

        TechSpecItemLevel2 output = new TechSpecItemLevel2()
        output.techSpecItem = techSpecItemV2
        output.numeroLinea = input.numeroLinea
        output.codigo = input.codigo
        output.descripcion = input.descripcion
        output.cantidad = input.cantidad
        output.medida = input.medida
        output.glosa = input.glosa
        output.codigoSes = input.codigoSes

        baseTransform(output)

        return output
    }
}
