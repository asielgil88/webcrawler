package util

import java.util.concurrent.ThreadPoolExecutor

/**
 * Created by asiel on 8/12/15.
 */

public class MyMonitorThread implements Runnable
{
    private ThreadPoolExecutor executor

    private int seconds

    private String processName

    private boolean run=true

    public MyMonitorThread(ThreadPoolExecutor executor, int delay, String processName)
    {
        this.executor = executor
        this.seconds= delay
        this.processName = processName
    }

    public void shutdown(){
        monitorPrint()
        this.run=false
    }

    public void monitorPrint(){
        System.out.println(
                String.format("[monitor] [%s] [%d/%d] Active: %d, Completed: %d, Task: %d, isShutdown: %s, isTerminated: %s",
                        this.processName,
                        this.executor.getPoolSize(),
                        this.executor.getCorePoolSize(),
                        this.executor.getActiveCount(),
                        this.executor.getCompletedTaskCount(),
                        this.executor.getTaskCount(),
                        this.executor.isShutdown(),
                        this.executor.isTerminated()))
    }

    @Override
    public void run()
    {
        while(run){
            monitorPrint()
            try {
                Thread.sleep(seconds*1000)
            } catch (InterruptedException e) {
                e.printStackTrace()
            }
        }

    }
}
