package util.handler

import grails.util.Holders

import model.v2.Category
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes
import scrap.ScrapService
import scrap.TaskManagerService
import util.MyMonitorThread
import util.RejectedExecutionHandlerImpl
import util.thread.Task1Thread

import java.util.concurrent.*

/**
 * Created by asiel on 8/12/15.
 */
class Task1Handler extends Thread {

    private int thread_limit = 0
    def ctx = ServletContextHolder.servletContext.getAttribute(GrailsApplicationAttributes.APPLICATION_CONTEXT)
    TaskManagerService taskManagerService = ctx.taskManagerService
    ScrapService scrapService = ctx.scrapService
    List<Category> categoryV2List

    Task1Handler(List<Category> categoryV2List) {
        thread_limit = new Integer(Holders.config.grails.scrap.thread.limit)
        this.categoryV2List = categoryV2List
    }

    private void execute() {

        RejectedExecutionHandlerImpl rejectionHandler = new RejectedExecutionHandlerImpl()
        ThreadFactory threadFactory = Executors.defaultThreadFactory()
        ThreadPoolExecutor executorPool = new ThreadPoolExecutor(thread_limit, thread_limit, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(), threadFactory, rejectionHandler)
        MyMonitorThread monitor = new MyMonitorThread(executorPool, 3, "Task 1")
        Thread monitorThread = new Thread(monitor)
        monitorThread.start()

        for (int i = 0; i < categoryV2List.size(); i++) {
            executorPool.execute(new Task1Thread(scrapService, categoryV2List.get(i)))
        }

        executorPool.shutdown()
        while (!executorPool.isTerminated()) {
        }
        monitor.shutdown()

        taskManagerService.runTask2()

    }

    @Override
    void run() {
        execute()
    }
}
