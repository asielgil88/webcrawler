package util.handler

import grails.util.Holders
import model.v2.OtherContract
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes
import scrap.ScrapService
import scrap.TaskManagerService
import util.MyMonitorThread
import util.RejectedExecutionHandlerImpl
import util.thread.Task0Thread

import java.util.concurrent.*


/**
 * Created by asiel on 8/12/15.
 */
class Task0Handler extends Thread {

    private int thread_limit = 0
    private boolean othersFeatureEnabled = false
    def ctx = ServletContextHolder.servletContext.getAttribute(GrailsApplicationAttributes.APPLICATION_CONTEXT)
    TaskManagerService taskManagerService = ctx.taskManagerService
    ScrapService scrapService = ctx.scrapService
    List<OtherContract> otherContractList

    Task0Handler(List<OtherContract> otherContractList) {       
        thread_limit = new Integer(Holders.config.grails.scrap.thread.limit)
        othersFeatureEnabled = new Boolean(Holders.config.grails.others.feature.enable)
        this.otherContractList = otherContractList
    }

    private void execute() {

        RejectedExecutionHandlerImpl rejectionHandler = new RejectedExecutionHandlerImpl()
        ThreadFactory threadFactory = Executors.defaultThreadFactory()
        ThreadPoolExecutor executorPool = new ThreadPoolExecutor(thread_limit, thread_limit, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(), threadFactory, rejectionHandler)
        MyMonitorThread monitor = new MyMonitorThread(executorPool, 3, "Task 0")
        Thread monitorThread = new Thread(monitor)
        monitorThread.start()

        if(otherContractList && othersFeatureEnabled) {
            for (int i = 0; i < otherContractList.size(); i++) {
                executorPool.execute(new Task0Thread(scrapService, otherContractList.get(i)))
            }
        }

        executorPool.shutdown()
        while (!executorPool.isTerminated()) {
        }
        monitor.shutdown()

        taskManagerService.runTask1()

    }

    @Override
    void run() {
        execute()
    }
}
