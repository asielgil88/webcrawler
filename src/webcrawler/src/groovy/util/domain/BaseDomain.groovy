package util.domain

class BaseDomain {

    static boolean compare2List(def list1, def list2) {

       list1 = list1.toArray().sort { it.toString() }
       list2 = list2.toArray().sort { it.toString() }

        if (list1.size() == list2.size()) {
            for (int i = 0; i < list1.size(); i++) {
                if (!list1[i].compare(list2[i])) {
                    return false
                }
            }
            return true
        }
        return false
    }

}
