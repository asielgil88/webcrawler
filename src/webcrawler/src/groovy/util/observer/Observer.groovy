package util.observer

/**
 * Created by asiel on 9/3/15.
 */
interface Observer {

    //method to update the observer, used by subject
    public void update();

    //attach with subject to observe
    public void setSubject(Subject sub);

}
