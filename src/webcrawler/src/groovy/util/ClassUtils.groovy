package util;

import java.lang.reflect.Field

class ClassUtils {

    static final String ID_FIELD_NAME = "id"
    static final String VERSION_FIELD_NAME = "version"

    static void cleanStringFields(Object obj) {

        Field[] fields = obj.getClass().getDeclaredFields()
        for (Field f : fields) {
            Class t = f.getType()
            if (t == String.class) {

            }
            if (f.getType() == String.class && f.getName() != ID_FIELD_NAME && f.getName() != VERSION_FIELD_NAME) {
                f.setAccessible(true)
                String cleanValue = removeNonLatinCharacters(f.get(obj))
                f.set(obj, cleanValue)
            }
        }
    }

    static String removeNonLatinCharacters(String s) {
        if (s != null) {
            StringBuilder out = new StringBuilder(Math.max(16, s.length()))
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i)
                if ((c >= 1 && c <= 31) || ((c >= 127 && c <= 159) || c == '�')) {
                    out.append("")
                } else {
                    out.append(c)
                }
            }
            return out.toString()
        } else {
            return s
        }
    }

}