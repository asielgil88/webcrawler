package util.thread

import model.v2.Act
import model.v2.Category
import model.v2.bidding.Bidding
import scrap.ScrapService

/**
 * Created by asiel on 8/12/15.
 */
class Task2Thread implements Runnable {

    Category categoryV2
    ScrapService scrapService

    Task2Thread(ScrapService scrapService, Category categoryV2) {
        this.categoryV2 = categoryV2
        this.scrapService = scrapService
    }

    @Override
    void run() {
        println(Thread.currentThread().getName() + " Thread Task2 --- " + toString() + " Start")

        scrapService.processActs(categoryV2)

        println(Thread.currentThread().getName() + " Thread Task2 --- " + toString() + " End")
    }


    @Override
    String toString() {
        return this.getClass().toString() + " >>>>>>>> " + categoryV2.nivel1
    }
}