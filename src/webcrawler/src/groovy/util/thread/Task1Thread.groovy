package util.thread

import model.v2.Category
import scrap.ScrapService

/**
 * Created by asiel on 8/12/15.
 */
class Task1Thread implements Runnable {

    Category categoryV2
    ScrapService scrapService

    Task1Thread(ScrapService scrapService, Category categoryV2) {
        this.categoryV2 = categoryV2
        this.scrapService = scrapService
    }

    @Override
    void run() {
        println(Thread.currentThread().getName() + " Thread Task1 --- " + toString() + " Start")

        scrapService.fetchListActos(categoryV2, scrapService.INICIO_LIST_ACTOS)

        println(Thread.currentThread().getName() + " Thread Task1 --- " + toString() + " End")
    }


    @Override
    String toString() {
        return this.getClass().toString() + " >>>>>>>> " + categoryV2.nivel1
    }
}