package util.thread

import model.v2.OtherContract
import scrap.ScrapService

/**
 * Created by asiel on 8/12/15.
 */
class Task0Thread implements Runnable {

    OtherContract otherContract
    ScrapService scrapService

    Task0Thread(ScrapService scrapService, OtherContract otherContract) {
        this.scrapService = scrapService
        this.otherContract = otherContract
    }

    @Override
    void run() {
        println(Thread.currentThread().getName() + " Thread Task0 --- " + toString() + " Start")

        scrapService.fetchOthersListActos(otherContract)

        println(Thread.currentThread().getName() + " Thread Task0 --- " + toString() + " End")
    }


    @Override
    String toString() {
        if(otherContract)
            return this.getClass().toString() + " >>>>>>>> " + otherContract?.name
        return ""
    }
}