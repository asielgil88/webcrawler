package util;

public class UrlConstants {

    public static final String URL_BASE = "http://www.panamacompra.gob.pa/";
    public static final String URL_LICITACION_LINK = URL_BASE + "Inicio/#!/vistaPreviaCP?NumLc=%s&esap=1&nnc=0&it=1";

}
