package retrofit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import java.util.concurrent.TimeUnit;
import util.UrlConstants;

public class ServiceUtil {

    private static long CONNECT_TIMEOUT = 60;
    private static long READ_TIMEOUT = 60;
    private static Retrofit retrofit;

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            OkHttpClient.Builder client = new OkHttpClient()
                    .newBuilder()
                    .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                    .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);

            retrofit = new Retrofit.Builder()
                    .client(client.build())
                    .baseUrl(UrlConstants.URL_BASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static PanamaCompraApi getPanamaCompraApi() {
        PanamaCompraApi service = getRetrofitInstance().create(PanamaCompraApi.class);
        return service;
    }

}
