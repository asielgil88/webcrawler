package retrofit;

import model.v2.Category;
import retrofit.response.ListActosEnvelopeResponse;
import retrofit.response.lista.dependencias.ListarDependenciasEnvelopeResponse;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface PanamaCompraApi {

    @GET("/Security/AmbientePublico.asmx/cargarOportunidadesDeNegocio")
    Call<List<Category>> loadCategories();

    @FormUrlEncoded
    @POST("/Security/AmbientePublico.asmx/cargarActosOportunidadesDeNegocio")
    Call<ListActosEnvelopeResponse> loadListActos(@Field("METHOD") String method, @Field("VALUE") String jsonBody);

    @FormUrlEncoded
    @POST("/Security/pliegoCargo.asmx/ListarDependencias")
    Call<ListarDependenciasEnvelopeResponse> loadListarDependencias(@Field("METHOD") String method, @Field("VALUE") String jsonBody);

}
