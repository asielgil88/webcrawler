package retrofit.response;

import model.v2.Act;

import java.util.List;

public class ListActosEnvelopeResponse {

    public int errorCode;
    public String headerDate;
    public String dateConsult;
    public int isNext;
    public String Consecutivo;
    public List<Act> listActos;
}

