package retrofit.response.lista.dependencias;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TechSpecItemV2Response {

    public String title;
    @SerializedName("type")
    public String typeValue;

    public ArrayList<TechSpecItemLevel2Response> values;

}
