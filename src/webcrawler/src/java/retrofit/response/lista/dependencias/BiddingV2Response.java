package retrofit.response.lista.dependencias;

public class BiddingV2Response {

    public BiddingHeaderResponse header;
    public BiddingFieldResponse<LabelV2Response> entityInformation;
    public BiddingFieldResponse<LabelV2Response> contactUser;
    public BiddingFieldResponse<LabelV2Response> aviso;
    public BiddingFieldResponse<DocAttachedV2Response> documentosAdjuntos;
    public TechSpecV2Response especificacionesTecnicas;

}
