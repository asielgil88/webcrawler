package retrofit.response.lista.dependencias;

public class ListarDependenciasEnvelopeResponse {

    public String errorCode;
    public String headerDate;
    public String dateConsult;
    public int isNext;
    //Not Parseable value
    //"listActos": null,
    public BiddingV2Response listNews;

}
