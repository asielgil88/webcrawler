package retrofit.response.lista.dependencias;

import com.google.gson.annotations.SerializedName;

public class TechSpecItemLevel2Response {

    @SerializedName("NumeroLinea")
    public int numeroLinea;
    @SerializedName("Codigo")
    public long codigo;
    @SerializedName("Descripcion")
    public String descripcion;
    @SerializedName("Cantidad")
    public String cantidad;
    @SerializedName("Medida")
    public String medida;
    @SerializedName("Glosa")
    public String glosa;
    @SerializedName("CodigoSes")
    public String codigoSes;

}
