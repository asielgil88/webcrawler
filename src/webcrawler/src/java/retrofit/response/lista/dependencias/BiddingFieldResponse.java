package retrofit.response.lista.dependencias;

import java.util.List;

public class BiddingFieldResponse<T> {

    public String title;
    public List<T> values;

}
