package retrofit.response.lista.dependencias;

import com.google.gson.annotations.SerializedName;

public class DocAttachedV2Response {

    public int tipoFiltro;
    public String link;
    public String comentario;
    @SerializedName("Descripcion")
    public String descripcion;
    @SerializedName("FechaCreacion")
    public String fechaCreacion;

}
