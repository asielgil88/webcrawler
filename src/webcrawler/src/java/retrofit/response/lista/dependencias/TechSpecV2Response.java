package retrofit.response.lista.dependencias;

import java.util.List;

public class TechSpecV2Response {

    public String title;

    public List<TechSpecItemV2Response> values;
}
