package webcrawler_v2


class SyncDataJob {

    def taskManagerService

    static triggers = {
        simple startDelay: 5 * 1000l, repeatInterval: 60 * 30 * 1000l // execute job once in 60 minutes
    }

    def execute() {
        // execute job
        syncData()
    }

    def syncData(){
        println("Starting  SyncDataJob at " + new Date().toString())
        if (!taskManagerService.threadIsAlive)
            taskManagerService.start()
    }
}
