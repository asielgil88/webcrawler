package webcrawler_v2

import model.v2.bidding.Bidding

class WatchListJob {
    def scrapService
    def myMailProviderService

    static triggers = {
        cron name: 'myTrigger', cronExpression: "0 0 0,12,16 * * ?"
    }

    def execute() {
        // execute job
        updateWatchList()
    }

    def updateWatchList(){
        def biddings = Bidding.findAllByObservada(true)
        List bidUpdates = []
        for (Bidding bid : biddings) {
            Bidding biddingUpdated
            if(bid.category != null) {
                biddingUpdated = scrapService.fetchListDependencias(bid.category, bid.numero)
            } else {
                biddingUpdated = scrapService.fetchListDependenciasByContract(bid.otherContract, bid.numero)
            }
            if (biddingUpdated) {
                if (bid.isModelChanged(biddingUpdated)) {
                    myMailProviderService.watchAlert(bid)
                    bidUpdates << bid
                }
            }
        }

        if (bidUpdates.size() > 0) {
//            myMailProviderService.watchListAlert(bidUpdates)
            bidUpdates.each {
                println("Updating Watchlist Bidding >>>>>>> " + it.numero)
            }
        }

        println("Finishing  SyncWatchlistTaskJob at " + new Date().toString())
    }
}
