package model.v2

import com.google.gson.annotations.SerializedName
import jline.internal.Nullable

class Act {

    Date dateCreated
    Date lastUpdated

    @SerializedName("NumeroAdquisicion")
    String numeroAdquisicion
    @SerializedName("DescripcionAdquisicion")
    String descripcionAdquisicion
    @SerializedName("NombreUnidadCompra")
    String nombreUnidadCompra
    @SerializedName("NombreDependencia")
    String nombreDependencia
    @SerializedName("Estado")
    String estado
    int nnc
    @SerializedName("IdEmpresa")
    String idEmpresa
    String modalidad
    @SerializedName("MontoRef")
    float montoRef
    String fecha

    //Extra Data
    boolean processed = false

    @Nullable
    Category category
    @Nullable
    OtherContract otherContract

    static constraints = {
        numeroAdquisicion unique: true
        category nullable: true
        otherContract nullable: true
    }

    static mapping = {
        category lazy: false
        descripcionAdquisicion sqlType: 'longText'
    }
}
