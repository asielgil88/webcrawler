package model.v2

import com.google.gson.annotations.SerializedName

class Category {

    String nivel1
    String idRubro
    @SerializedName("PrimerNivel")
    String primerNivel
    @SerializedName("Total")
    String total

    //Extra Data
    Date lastUpdated
    boolean enabled = false

    static constraints = {
        primerNivel unique: true
    }

    def updateModel(Category item) {
        this.nivel1 = item.nivel1
        this.idRubro = item.idRubro
        this.primerNivel = item.primerNivel
        this.total = item.total
        save()
    }

    int countBidToday() {
        Date today = new Date().clearTime()
        Date todayEnd = today + 1
        def query = Act.withCriteria() {
            eq("category", this)
//            eq("processed", true)
            lt("dateCreated", todayEnd)
            ge("dateCreated", today)
            projections {
                count "id", 'mycount'
            }
        }
        return query[0]
    }

    int countBidTotal() {
        def query = Act.withCriteria() {
            eq("category", this)
//            eq("processed", true)
            projections {
                count "id", 'mycount'
            }
        }
        return query[0]
    }
}
