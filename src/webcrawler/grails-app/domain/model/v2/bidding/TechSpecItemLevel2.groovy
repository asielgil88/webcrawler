package model.v2.bidding

import groovy.json.StringEscapeUtils
import groovy.transform.ToString

import util.domain.DomainInterface

@ToString(includeNames = true, includeFields = true, excludes = "id,version,techSpec")
class TechSpecItemLevel2 implements DomainInterface<TechSpecItemLevel2>{

    int numeroLinea
    long codigo
    String descripcion
    String cantidad
    String medida
    String glosa
    String codigoSes

    static belongsTo = [techSpecItem: TechSpecItem]

    static constraints = {
        numeroLinea nullable: true
        codigoSes nullable: true
        descripcion nullable: true
        cantidad nullable: true
        medida nullable: true
        glosa nullable: true
        codigoSes nullable: true
    }

    boolean compare(TechSpecItemLevel2 other) {
        String json1 = getString()
        String json2 = other.getString()

        return json1.equals(json2)
    }

    String getString(){
        return StringEscapeUtils.escapeJava(toString())
    }
}
