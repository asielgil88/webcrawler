package model.v2.bidding

import groovy.json.StringEscapeUtils
import groovy.transform.ToString

import util.domain.DomainInterface

@ToString(includeNames = true, includeFields = true, excludes = "id,version")
class Label implements DomainInterface<Label>{

    String label
    String value_field

    static constraints = {
        value_field nullable: true
    }

    static mapping = {
        value_field sqlType: 'longText', nullable: true
    }

    boolean compare(Label other) {
        String json1 = this.getString()
        String json2 = other.getString()
        return json1.equals(json2)
    }

    String getString(){
        return StringEscapeUtils.escapeJava(toString())
    }

}
