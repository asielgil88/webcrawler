package model.v2.bidding

import groovy.json.StringEscapeUtils
import groovy.transform.ToString
import jline.internal.Nullable
import model.v2.Alert
import model.v2.Category
import model.v2.OtherContract
import org.apache.commons.lang.StringUtils
import util.ClassUtils
import util.StringConstants
import util.domain.BaseDomain
import util.domain.DomainInterface

@ToString(includeNames = true, includeFields = true, excludes = "id, version, dateCreated, myMailProviderService, category, observada, lastUpdated, especificacionesTecnicas")
class Bidding implements DomainInterface<Bidding> {

    def myMailProviderService

    Date dateCreated
    Date lastUpdated
    @Nullable
    Category category
    @Nullable
    OtherContract otherContract
    boolean observada

    //Header
    String numero
    String descripcion
    String objetoContratacion
    //Entidad
    String entidad
    String dependencias
    String unidadDeCompra
    String direccion
    //ContactUser
    String contactUserNombre
    String contactUserCargo

    TechSpec especificacionesTecnicas

    static hasMany = [documentosAdjuntos: DocAttached, avisos: Label]

    static constraints = {
        numero unique: true
        category nullable: true
        otherContract nullable: true
    }

    static mapping = {
        numero sqlType: 'text', nullable: true
        descripcion sqlType: 'longText', nullable: true
        objetoContratacion sqlType: 'longText', nullable: true
        entidad sqlType: 'longText', nullable: true
        dependencias sqlType: 'longText', nullable: true
        unidadDeCompra sqlType: 'longText', nullable: true
        direccion sqlType: 'longText', nullable: true
        contactUserNombre sqlType: 'longText', nullable: true
        contactUserCargo sqlType: 'longText', nullable: true

        especificacionesTecnicas lazy: false
        documentosAdjuntos lazy: false
        avisos lazy: false
        category lazy: false
    }

    boolean isModelChanged(Bidding source) {
        if (!source.compare(this)) {
            def diff = []
            String json1 = getString()
            String json2 = source.getString()

            boolean docAreEqueals = BaseDomain.compare2List(documentosAdjuntos, source.documentosAdjuntos)

            boolean avisosEqueals = BaseDomain.compare2List(avisos, source.avisos)

            if (!json1.equals(json2)) {
                numero = source.numero
                descripcion = source.descripcion
                objetoContratacion = source.objetoContratacion
                entidad = source.entidad
                dependencias = source.dependencias
                unidadDeCompra = source.unidadDeCompra
                direccion = source.direccion
                contactUserNombre = source.contactUserNombre
                contactUserCargo = source.contactUserCargo
                diff << "Informacion Basica de Licitacion Actualizadas"

            }

            if (!docAreEqueals) {
                documentosAdjuntos.each {
                    it.delete()
                }
                documentosAdjuntos = source.documentosAdjuntos
                diff << "Documentos Adjuntos Actualizados"
            }

            if (!avisosEqueals) {
                avisos.each {
                    it.delete()
                }
                avisos = source.avisos
                diff << "Avisos Actualizados"
            }

            if (!especificacionesTecnicas.compare(source.especificacionesTecnicas)) {
                especificacionesTecnicas = source.especificacionesTecnicas
                diff << "Especificaciones Tecnicas Actualizadas"
            }

            println("Updating Model Difference >>>>>> " + diff)

            this.saveProcedure()
            return true
        }
        return false
    }

    void saveProcedure() {
        if (this.validate()) {
            if (especificacionesTecnicas && especificacionesTecnicas.validate())
                especificacionesTecnicas.save()
            this.save()
            println("Saving Bid >>>>>> " + this.numero)
        }
    }

    void saveValidation() {
        def biddingExits = Bidding.findByNumero(numero)
        if (biddingExits) {
            println("existing bid " + numero)
            biddingExits.isModelChanged(this)
        } else {
            saveProcedure()
        }
    }

    boolean compare(Bidding other) {
        String json1 = getString()
        String json2 = other.getString()

        boolean docAreEqueals = BaseDomain.compare2List(documentosAdjuntos, other.documentosAdjuntos)

        boolean avisosEqueals = BaseDomain.compare2List(avisos, other.avisos)

        return json1.equals(json2) && docAreEqueals && avisosEqueals && especificacionesTecnicas.compare(other.especificacionesTecnicas)
    }

    def afterInsert() {
        def wordList = Alert.getAll()
        wordList.each {
            if (StringUtils.containsIgnoreCase(descripcion, it.termino)
                    || especificacionesTecnicas.containsTerm(it.termino)) {
                myMailProviderService.insertAlert(this, it.termino)
            }
        }
    }

    def getPanamaLink() {
        return String.format(util.UrlConstants.URL_LICITACION_LINK, numero)
    }

    String getString() {
        return StringEscapeUtils.escapeJava(toString())
    }

    def getFechaPublicacion() {
        def fecha = ""
        avisos.each {
            if (it.label.contains(StringConstants.FECHA_PUBLICACION_CONST)) {
                fecha = it.value_field
            }
        }
        return fecha
    }

    def getFechaRecepcion() {
        def fecha = null
        avisos.each {
            if (it.label.contains(StringConstants.FECHA_RECEPCION_CONST)) {
                fecha = it.value_field
            }
        }
        return fecha
    }

    def getFechaPresentacion() {
        def fecha = null
        avisos.each {
            if (it.label.contains(StringConstants.FECHA_PRESENTACION_CONST)) {
                fecha = it.value_field
            }
        }
        return fecha
    }

    def cleanAllStrings(){
        ClassUtils.cleanStringFields(this)
        for(DocAttached item: documentosAdjuntos){
            ClassUtils.cleanStringFields(item)
        }
        for(Label item: avisos){
            ClassUtils.cleanStringFields(item)
        }
        ClassUtils.cleanStringFields(especificacionesTecnicas)
        for (TechSpecItem item: especificacionesTecnicas.values){
            ClassUtils.cleanStringFields(item)
            for (TechSpecItemLevel2 item2: item.values){
                ClassUtils.cleanStringFields(item2)
            }
        }
        Bidding.withTransaction {
            saveValidation()
        }
    }

}
