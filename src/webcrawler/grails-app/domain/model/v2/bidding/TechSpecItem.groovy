package model.v2.bidding

import groovy.json.StringEscapeUtils
import groovy.transform.ToString
import util.domain.BaseDomain
import util.domain.DomainInterface

@ToString(includeNames = true, includeFields = true, excludes = "id,version,techSpec")
class TechSpecItem implements DomainInterface<TechSpecItem>{

    String title
    String typeValue

    static belongsTo = [techSpec: TechSpec]

    static hasMany = [values: TechSpecItemLevel2]

    static mapping = {
        values lazy: false
    }

    static constraints = {
        typeValue nullable: true
        title nullable: true
    }

    boolean compare(TechSpecItem other) {
        String json1 = getString()
        String json2 = other.getString()

        boolean valuesEquals = BaseDomain.compare2List(values, other.values)

        return json1.equals(json2) && valuesEquals
    }

    String getString(){
        return StringEscapeUtils.escapeJava(toString())
    }
}
