package model.v2.bidding

import com.google.gson.annotations.SerializedName
import groovy.json.StringEscapeUtils
import groovy.transform.ToString

import util.domain.DomainInterface

@ToString(includeNames = true, includeFields = true, excludes = "id,version,link")
class DocAttached implements DomainInterface<DocAttached>{

    int tipoFiltro
    //plus base URL
    String link
    String comentario
    @SerializedName("Description")
    String description
    @SerializedName("FechaCreacion")
    String fechaCreacion

    static constraints = {
    }

    static mapping = {
        link sqlType: 'longText', nullable: true
    }

    boolean compare(DocAttached other) {
        String json1 = getString()
        String json2 = other.getString()

        //Handle link updates with no report
        if(!link.equals(other.link)){
            link = other.link
            save()
        }

        return json1.equals(json2)
    }

    String getString(){
        return StringEscapeUtils.escapeJava(toString())
    }

    def getPanamaLink() {
        return util.UrlConstants.URL_BASE.concat(link)
    }

}
