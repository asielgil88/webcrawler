package model.v2.bidding

import groovy.json.StringEscapeUtils
import groovy.transform.ToString
import org.apache.commons.lang.StringUtils
import util.domain.BaseDomain
import util.domain.DomainInterface

@ToString(includeNames = true, includeFields = true, excludes = "id,version,bidding")
class TechSpec implements DomainInterface<TechSpec> {

    String title

    static belongsTo = [bidding: Bidding]

    static hasMany = [values: TechSpecItem]

    static constraints = {
        title nullable: true
    }

    static mapping = {
        values lazy: false
    }

    boolean compare(TechSpec other) {
        String json1 = getString()
        String json2 = other.getString()

        boolean valuesEquals = BaseDomain.compare2List(values, other.values)

        return json1.equals(json2) && valuesEquals
    }

    def getAllItems() {
        def list = []
        values.each {
            it.values.each {
                list << it
            }
        }
        return list
    }

    String getString() {
        return StringEscapeUtils.escapeJava(toString())
    }

    boolean containsTerm(String term) {

        for (TechSpecItem item : values) {
            if (StringUtils.containsIgnoreCase(item.toString(), term)) {
                return true
            }
            for(TechSpecItemLevel2 item2 : item.values){
                if (StringUtils.containsIgnoreCase(item2.toString(), term)) {
                    return true
                }
            }
        }

        return false
    }
}
