package model.v2

class OtherContract {

    String name
    String jsonRquest

    static constraints = {
    }

    int countBidToday() {
        Date today = new Date().clearTime()
        Date todayEnd = today + 1
        def query = Act.withCriteria() {
            eq("otherContract", this)
//            eq("processed", true)
            lt("dateCreated", todayEnd)
            ge("dateCreated", today)
            projections {
                count "id", 'mycount'
            }
        }
        return query[0]
    }

    int countBidTotal() {
        def query = Act.withCriteria() {
            eq("otherContract", this)
//            eq("processed", true)
            projections {
                count "id", 'mycount'
            }
        }
        return query[0]
    }
}
