package model.v2

class Contact {

    String name
    String email

    static constraints = {
        email email: true
    }
}
