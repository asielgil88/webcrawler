package model

import org.apache.commons.lang.builder.HashCodeBuilder

class SysUserRole implements Serializable {

	private static final long serialVersionUID = 1

	SysUser sysUser
	Role role

	boolean equals(other) {
		if (!(other instanceof SysUserRole)) {
			return false
		}

		other.sysUser?.id == sysUser?.id &&
		other.role?.id == role?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (sysUser) builder.append(sysUser.id)
		if (role) builder.append(role.id)
		builder.toHashCode()
	}

	static SysUserRole get(long sysUserId, long roleId) {
		SysUserRole.where {
			sysUser == SysUser.load(sysUserId) &&
			role == Role.load(roleId)
		}.get()
	}

	static boolean exists(long sysUserId, long roleId) {
		SysUserRole.where {
			sysUser == SysUser.load(sysUserId) &&
			role == Role.load(roleId)
		}.count() > 0
	}

	static SysUserRole create(SysUser sysUser, Role role, boolean flush = false) {
		def instance = new SysUserRole(sysUser: sysUser, role: role)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(SysUser u, Role r, boolean flush = false) {
		if (u == null || r == null) return false

		int rowCount = SysUserRole.where {
			sysUser == SysUser.load(u.id) &&
			role == Role.load(r.id)
		}.deleteAll()

		if (flush) { SysUserRole.withSession { it.flush() } }

		rowCount > 0
	}

	static void removeAll(SysUser u, boolean flush = false) {
		if (u == null) return

		SysUserRole.where {
			sysUser == SysUser.load(u.id)
		}.deleteAll()

		if (flush) { SysUserRole.withSession { it.flush() } }
	}

	static void removeAll(Role r, boolean flush = false) {
		if (r == null) return

		SysUserRole.where {
			role == Role.load(r.id)
		}.deleteAll()

		if (flush) { SysUserRole.withSession { it.flush() } }
	}

	static constraints = {
		role validator: { Role r, SysUserRole ur ->
			if (ur.sysUser == null) return
			boolean existing = false
			SysUserRole.withNewSession {
				existing = SysUserRole.exists(ur.sysUser.id, r.id)
			}
			if (existing) {
				return 'userRole.exists'
			}
		}
	}

	static mapping = {
		id composite: ['role', 'sysUser']
		version false
	}
}
