package model.v2

import grails.plugin.springsecurity.annotation.Secured

@Secured(["ROLE_ADMIN", "ROLE_USER"])
class OthersController {

    def scrapService

    def index() {
        def query = OtherContract.findAll()
        if (params.message)
            flash.error = params.message

        respond query
    }
}
