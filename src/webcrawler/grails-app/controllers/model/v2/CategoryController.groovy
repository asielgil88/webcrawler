package model.v2

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

class CategoryController {

    def scrapService

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def index(Integer max) {
        //TODO Offset and Order breaking query increasing max to list them all
        params.max = Math.min(max ?: 100, 100)
        params.offset = params.offset ?: 0
        def criteria = Category.createCriteria()
        def query = criteria.list(max: params.max, offset: params.offset) {
            order("enabled", "desc")
        }

        if (params.message)
            flash.error = params.message

        respond query, model: [totalCount: Category.count()]
    }

    @Secured(["ROLE_ADMIN"])
    def categoryRefresh() {
        scrapService.fetchCategories()
        flash.message = "Category Refresh started this may take a while!"
        redirect(action: 'index')
    }

    @Secured(["ROLE_ADMIN"])
    @Transactional
    def enableCategoria(int id) {
        Category categoryV2 = Category.findById(id)
        def map = [:]

        if (categoryV2) {
            categoryV2.enabled = true
            categoryV2.save(flush: true)
            map.message = "category enabled"
        } else {
            response.status = 404
            map.error = "category not found"
        }

        render map as JSON
    }

    @Secured(["ROLE_ADMIN"])
    @Transactional
    def disableCategoria(int id) {
        Category categoryV2 = Category.findById(id)
        def map = [:]

        if (categoryV2) {
            categoryV2.enabled = false
            categoryV2.save(flush: true)
            map.message = "category disabled"
        } else {
            response.status = 404
            map.error = "category not found"
        }

        render map as JSON
    }

}
