package model.v2

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import model.Role
import model.SysUser
import model.SysUserRole
import model.v2.bidding.Bidding

import static org.springframework.http.HttpStatus.NOT_FOUND

@Transactional(readOnly = true)
class BiddingController {

    def scrapService
    def springSecurityService
    def myMailProviderService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured("permitAll")
    def showPublic(Bidding biddingV2Instance) {

        if(springSecurityService.isLoggedIn()){
            redirect(action: 'show', id: biddingV2Instance.id)
        }else{
            respond biddingV2Instance
        }

    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = params.offset ?: 0

        def categoryID = params.id
        def category = categoryID ? Category.get(categoryID) : null

        def criteria = Bidding.createCriteria()
        def query = criteria.list {
            if (category) {
                eq("category", category)
            }
            order("dateCreated", "desc")
            maxResults(new Integer(params.max))
            firstResult(new Integer(params.offset))
        }

        def queryCount = Bidding.withCriteria {
            if (category) {
                eq("category", category)
            }
            order("dateCreated", "desc")
            projections {
                countDistinct "id"
            }
        }

        if (params.message)
            flash.error = params.message

        respond query, model: [totalCount: queryCount[0], categoryID: categoryID]
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def getByOtherContract(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = params.offset ?: 0

        def otherContractID = params.id
        def otherContract = otherContractID ? OtherContract.get(otherContractID) : null

        def criteria = Bidding.createCriteria()
        def query = criteria.list {
            if (otherContract) {
                eq("otherContract", otherContract)
            }
            order("dateCreated", "desc")
            maxResults(new Integer(params.max))
            firstResult(new Integer(params.offset))
        }

        def queryCount = Bidding.withCriteria {
            if (otherContract) {
                eq("otherContract", otherContract)
            }
            order("dateCreated", "desc")
            projections {
                countDistinct "id"
            }
        }

        if (params.message)
            flash.error = params.message

        respond query, model: [totalCount: queryCount[0], otherContractID: otherContractID]
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def show(Bidding biddingV2Instance) {
        respond biddingV2Instance
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def search(Integer max, Integer offset, String q) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = offset ?: 0
        def query = new ArrayList()
        if (q) {
            def str = "%" + q + "%"
            def criteria = Bidding.createCriteria()
            query = criteria.list {

                or {
                    ilike('descripcion', str)
                    ilike('numero', str)
                }

                maxResults(params.max)
                firstResult(params.offset)
                order("dateCreated", "desc")
            }

            def queryTotal = Bidding.withCriteria {
                or {
                    ilike('descripcion', str)
                    ilike('numero', str)
                }
                projections {
                    countDistinct "id"
                }
            }

            if (queryTotal[0] == 0) {
                flash.error = "No results Found for your search criteria"
            }
            respond query, model: [totalCount: queryTotal[0], q: q]
        } else {
            respond query, model: [totalCount: 0]
        }

    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def watchlist(Integer max, Integer offset) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = offset ?: 0

        def query = new ArrayList()
        def criteria = Bidding.createCriteria()
        query = criteria.list {
            eq('observada', true)
            maxResults(params.max)
            firstResult(params.offset)
            order("dateCreated", "desc")
        }

        def queryTotal = Bidding.withCriteria {
            eq('observada', true)
            projections {
                countDistinct "id"
            }
        }

        respond query, model: [totalCount: queryTotal[0]]
    }

    @Secured("ROLE_ADMIN")
    @Transactional
    def migration1() {
        render scrapService.migrateWatchlist() as JSON
    }

//    @Secured("ROLE_ADMIN")
//    @Transactional
//    def debug1() {
//        // 2021-0-12-0-08-CL-033358
//        def contract = OtherContract.findByName("Mesa Conjunta MINSA-CSS")
//        def bid = scrapService.fetchListDependenciasByContract(contract, "2021-2-78-0-08-PE-013600")
//        println(bid)
//        Bidding.withTransaction {
//            bid.saveValidation()
//        }
//        render bid as JSON
//    }
//
//    @Secured("ROLE_ADMIN")
//    @Transactional
//    def debug2() {
//        // 2021-0-12-0-08-CL-033358
//        List bidUpdates = []
//        def contract = OtherContract.findByName("Mesa Conjunta MINSA-CSS")
//        def bidLocal = Bidding.findByNumero("2021-0-12-0-08-CL-033358")
//        def biddingUpdated = scrapService.fetchListDependenciasByContract(contract, "2021-0-12-0-08-CL-033358")
//        println(biddingUpdated)
//        if (biddingUpdated) {
//            if (bidLocal.isModelChanged(biddingUpdated)) {
//                bidUpdates << bidLocal
//            }
//        }
//
//        if (bidUpdates.size() > 0) {
////            myMailProviderService.watchListAlert(bidUpdates)
//            bidUpdates.each {
//                println("Updating Watchlist Bidding >>>>>>> " + it.numero)
//            }
//        }
//
//        render bidUpdates as JSON
//    }


//    @Secured("ROLE_ADMIN")
//    @Transactional
//    def migration2() {
//        render scrapService.migrateAlert() as JSON
//    }
//
//    @Secured("ROLE_ADMIN")
//    @Transactional
//    def migration3() {
//        render scrapService.migrateCleanWatchlist() as JSON
//    }

    @Secured("ROLE_ADMIN")
    @Transactional
    def migration4() {
        def userAuthority = Role.findByAuthority("ROLE_USER")
        def user = new SysUser()
        user.username = 'licitaciones1@lacomedicalgroup.com'
        user.password = 'lancotender2021'
        user.save(flush: true)
        SysUserRole.create(user, userAuthority, true)

        render user as JSON
    }

    @Secured("ROLE_ADMIN")
    @Transactional
    def testEmail() {
        myMailProviderService.reportEvent("Testing EMail")
        render "Success"
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    @Transactional
    def toggleWatch(Integer id) {
        Bidding bidding = Bidding.get(id)
        if (bidding) {
            bidding.observada = !bidding.observada
            bidding.save(flush: true)
        }
        redirect(action: "show", id: id)
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    protected void notFound() {
        request.withFormat  {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'biddingV2.label', default: 'Bidding'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

}
