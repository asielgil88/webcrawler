package model.v2

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

@Secured("ROLE_ADMIN")
class ContactController {

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = params.offset ?: 0

        def criteria = Contact.createCriteria()
        def query = criteria.list {
            maxResults(new Integer(params.max))
            firstResult(new Integer(params.offset))
        }

        if (params.message)
            flash.error = params.message

        respond query, model: [totalCount: Contact.count()]
    }

    def create() {
        def email = params.email
        def name = params.name
        if (email) {
            Contact Contact = new Contact(email: email, name: name)
            Contact.save()
            flash.message = "Contact Saved"
        } else {
            flash.error = "Missing Params"
        }
        redirect(action: 'index')
    }

    @Transactional
    def delete(Integer id) {
        Contact Contact = Contact.get(id)
        if (Contact) {
            Contact.delete()
            flash.message = "Contact Deleted"
        } else {
            flash.error = "Contact Not Found"
        }
        redirect(action: 'index')
    }
}
