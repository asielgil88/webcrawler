package model.v2

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

@Secured("ROLE_ADMIN")
class AlertController {

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.offset = params.offset ?: 0

        def criteria = Alert.createCriteria()
        def query = criteria.list {
            maxResults(new Integer(params.max))
            firstResult(new Integer(params.offset))
        }

        if (params.message)
            flash.error = params.message

        respond query, model: [totalCount: Alert.count()]
    }

    def create(){
        def termino =  params.termino
        if(termino){
            Alert alert = new Alert(termino: termino)
            alert.save()
            flash.message = "Alert Saved"
        }else{
            flash.error = "Missing Params"
        }
        redirect(action: 'index')
    }

    @Transactional
    def delete(Integer id){
        Alert alert = Alert.get(id)
        if(alert){
            alert.delete()
            flash.message = "Alert Deleted"
        }else{
            flash.error = "Alert Not Found"
        }
        redirect(action: 'index')
    }
}
