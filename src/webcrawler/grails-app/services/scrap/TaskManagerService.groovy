package scrap

import grails.transaction.Transactional
import model.v2.Category
import model.v2.OtherContract
import util.handler.Task0Handler
import util.handler.Task1Handler
import util.handler.Task2Handler

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

@Transactional
class TaskManagerService {

    public static boolean threadIsAlive = false
    public static boolean threadIsInit = false

    Task0Handler task0Handler
    Task1Handler task1Handler
    Task2Handler task2Handler

    def initIfNecesary() {
        if (!threadIsInit) {
            keepChecking()
            threadIsInit = true
        }

    }

    def start() {
        initIfNecesary()
        runTask0()
        threadIsAlive = true
    }

    def end() {
        threadIsAlive = false
    }

    @Transactional
    def runTask0() {
        ArrayList<OtherContract> otherContractList = OtherContract.findAll()
        task0Handler = new Task0Handler(otherContractList)
        task0Handler.start()
    }

    @Transactional
    def runTask1() {

        ArrayList<Category> categoryV2List = Category.findAllByEnabled(true)
        task1Handler = new Task1Handler(categoryV2List)
        task1Handler.start()

    }

    @Transactional
    def runTask2() {

        ArrayList<Category> categoryV2List = Category.findAllByEnabled(true)
        task2Handler = new Task2Handler(categoryV2List)
        task2Handler.start()

    }

    def keepChecking() {

        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor()
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (!task1Handler.isAlive() && !task2Handler.isAlive())
                    threadIsAlive = false
            }
        }, 0, 30, TimeUnit.SECONDS)

    }
}
