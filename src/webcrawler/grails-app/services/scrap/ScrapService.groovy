package scrap

import grails.transaction.Transactional
import model.v2.Act
import model.v2.Alert
import model.v2.Category
import model.v2.bidding.Bidding
import model.v2.OtherContract
import org.springframework.dao.CannotAcquireLockException
import org.springframework.dao.DeadlockLoserDataAccessException
import retrofit.PanamaCompraApi
import retrofit.ServiceUtil
import retrofit.response.ListActosEnvelopeResponse
import retrofit.response.lista.dependencias.BiddingV2Response
import retrofit.response.lista.dependencias.ListarDependenciasEnvelopeResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import util.mapper.BiddingMapper

@Transactional
class ScrapService {

    def myMailProviderService

    public static final int INICIO_LIST_ACTOS = 0
    public static final String INICIO_LIST_OTHERS = "0"
    public static final String LIST_ACTOS_METHOD = "0"
    public static String LIST_ACTOS_VALUE = "{\"BusquedaRubros\":\"%s\",\"IdRubro\":\"%s\",\"Inicio\":%d}"

    public static final String LISTAR_DEPENDENCIAS_METHOD = "1"
    public static String LISTAR_DEPENDENCIAS_VALUE = "{\"NumLc\":\"%s\",\"esap\":\"1\",\"nnc\":\"0\",\"it\":\"1\"}"

    def fetchCategories() {

        PanamaCompraApi panamaCompraApi = ServiceUtil.getPanamaCompraApi()
        Call<List<Category>> callToExecute = panamaCompraApi.loadCategories()
        callToExecute.enqueue(new Callback<List<Category>>() {
            @Override
            void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()) {
                    Category.withTransaction {
                        response.body().each {
                            def categoryExist = Category.findByIdRubro(it.idRubro)
                            if (categoryExist) {
                                categoryExist.updateModel(it)
                            } else {
                                it.save()
                            }
                        }
                    }
                }
            }

            @Override
            void onFailure(Call<List<Category>> call, Throwable throwable) {
                println(throwable)
            }
        })
    }

    def fetchListActos(Category categoryV2, int inicio) {

        PanamaCompraApi panamaCompraApi = ServiceUtil.getPanamaCompraApi()
        String jsonBody = String.format(LIST_ACTOS_VALUE, "true", categoryV2.primerNivel, inicio ? inicio : INICIO_LIST_ACTOS)
        Call<ListActosEnvelopeResponse> callToExecute = panamaCompraApi.loadListActos(LIST_ACTOS_METHOD, jsonBody)
        def lastProcessing
        try {
            Response<ListActosEnvelopeResponse> response = callToExecute.execute()
            if (response.isSuccessful()) {
                boolean atLeastOneNew = false
                Act.withTransaction {
                    response.body().listActos.each {
                        lastProcessing = it
                        Act act = Act.findByNumeroAdquisicion(it.getNumeroAdquisicion())
                        if (!act) {
                            it.category = categoryV2
                            atLeastOneNew = true
                            println("Saving Act >>>>>> " + it.getNumeroAdquisicion())
                            if (it.validate())
                                it.save()
                        }
                    }
                }
                if (atLeastOneNew) {
                    fetchListActos(categoryV2, response.body().isNext)
                }
            }
        }
        catch (Exception ex) {
            println("Error in fetchListActos >>>> Category :" + categoryV2.primerNivel + " Inicio: " + inicio)
            if (onExeptionTakeAction(ex, "Act" + lastProcessing?.id )) {
                fetchListActos(categoryV2, inicio)
            }
        }

    }

    def fetchOthersListActos(OtherContract otherContract) {
        fetchOthers(otherContract, INICIO_LIST_OTHERS)
        processOtherContracts(otherContract)
    }

    private fetchOthers(OtherContract otherContract, String Consecutivo) {

        PanamaCompraApi panamaCompraApi = ServiceUtil.getPanamaCompraApi()
        String jsonBody = String.format(otherContract.jsonRquest, "true", Consecutivo ? Consecutivo : INICIO_LIST_ACTOS)
        Call<ListActosEnvelopeResponse> callToExecute = panamaCompraApi.loadListActos(LIST_ACTOS_METHOD, jsonBody)
        def lastProcessing
        try {
            Response<ListActosEnvelopeResponse> response = callToExecute.execute()
            if (response.isSuccessful()) {
                boolean atLeastOneNew = false
                Act.withTransaction {
                    response.body().listActos.each {
                        lastProcessing = it
                        Act act = Act.findByNumeroAdquisicion(it.getNumeroAdquisicion())
                        if (!act) {
                            it.otherContract = otherContract
                            atLeastOneNew = true
                            println("Saving Act >>>>>> " + it.getNumeroAdquisicion())
                            if (it.validate())
                                it.save()
                        }
                    }
                }
                if (atLeastOneNew) {
                    fetchOthers(otherContract, response.body().Consecutivo)
                }
            }
        }
        catch (Exception ex) {
            println("Error in fetchListActos >>>> Other :" + jsonBody + " Inicio: " + Consecutivo)
            if (onExeptionTakeAction(ex, "Act" + lastProcessing?.id )) {
                fetchOthers(otherContract, Consecutivo)
            }
        }

    }

    def processOtherContracts(OtherContract otherContract) {
        def actList = Act.withCriteria {
            eq('otherContract', otherContract)
            eq('processed', false)
            order("dateCreated", "desc")
        }
        def count = 0
        for (Act actV2 : actList) {
            println("Limit iteration counter ********* " + count)
            if (count < 50) {
                count += 1
                Bidding bidding = fetchListDependenciasByContract(otherContract, actV2.numeroAdquisicion)
                Bidding.withTransaction {
                    bidding.saveValidation()
                }
                actV2.processed = true
                actV2.save(flush: true)
                println("persisting other act " + actV2.numeroAdquisicion)
            } else {
                break
            }
        }
    }

    def processActs(Category category) {
        def actList = Act.withCriteria {
            eq('category', category)
            eq('processed', false)

        }
        for (Act actV2 : actList) {
            Bidding bidding = fetchListDependencias(actV2.category, actV2.numeroAdquisicion)
            println("starting saving bid " + bidding.numero)
            Bidding.withTransaction {
                bidding.saveValidation()
            }
            actV2.processed = true
            actV2.save(flush: true)
            println("persisting act " + actV2.numeroAdquisicion)
        }
    }

    def fetchListDependencias(Category category, String numeroAdquisicion) {

        PanamaCompraApi panamaCompraApi = ServiceUtil.getPanamaCompraApi()
        String jsonBody = String.format(LISTAR_DEPENDENCIAS_VALUE, numeroAdquisicion)
        Call<ListarDependenciasEnvelopeResponse> callToExecute = panamaCompraApi.loadListarDependencias(LISTAR_DEPENDENCIAS_METHOD, jsonBody)

        try {
            Response<ListarDependenciasEnvelopeResponse> response = callToExecute.execute()
            if (response.isSuccessful()) {
                BiddingV2Response biddingV2Response = response.body().listNews
                BiddingMapper mapper = new BiddingMapper()
                Bidding biddingV2 = mapper.transform(biddingV2Response)
                biddingV2.category = category
                return biddingV2
            }
        } catch (Exception ex) {
            println("Error in fetchListDependencias >>>> Act :" + numeroAdquisicion)
            if (onExeptionTakeAction(ex, "Bidding " + numeroAdquisicion)) {
                fetchListDependencias(category, numeroAdquisicion)
            }
        }
    }

    def fetchListDependenciasByContract(OtherContract contract, String numeroAdquisicion) {

        PanamaCompraApi panamaCompraApi = ServiceUtil.getPanamaCompraApi()
        String jsonBody = String.format(LISTAR_DEPENDENCIAS_VALUE, numeroAdquisicion)
        Call<ListarDependenciasEnvelopeResponse> callToExecute = panamaCompraApi.loadListarDependencias(LISTAR_DEPENDENCIAS_METHOD, jsonBody)

        try {
            Response<ListarDependenciasEnvelopeResponse> response = callToExecute.execute()
            if (response.isSuccessful()) {
                BiddingV2Response biddingV2Response = response.body().listNews
                BiddingMapper mapper = new BiddingMapper()
                Bidding biddingV2 = mapper.transform(biddingV2Response)
                biddingV2.otherContract = contract
                return biddingV2
            }
        } catch (Exception ex) {
            println("Error in fetchListDependencias >>>> Act :" + numeroAdquisicion)
            if (onExeptionTakeAction(ex, "Bidding " + numeroAdquisicion)) {
                fetchListDependenciasByContract(contract, numeroAdquisicion)
            }
        }
    }

    def migrateWatchlist() {
        String dataRaw = this.class.classLoader.getResourceAsStream('data_migration').getText()
        List lines = dataRaw.split('\n')

        List notFound = []
        List updated = []
        List migrated = []

        Bidding.withTransaction {
            lines.each {
                Bidding biddingExist = Bidding.findByNumero(it)
                if (!biddingExist) {
                    Bidding bidding = fetchListDependencias(null, it)
                    if (!bidding) {
                        notFound << it
                    } else {
                        bidding.observada = true
                        bidding.saveValidation()
                        migrated << it
                    }
                } else {
                    println(" Updated " + it)
                    biddingExist.observada = true
                    biddingExist.save()
                    updated << it
                }
            }
        }
        def map = [:]
        map.notFound = notFound
        map.updated = updated
        map.migrated = migrated

        return map
    }

    def migrateAlert() {
        String dataRaw = this.class.classLoader.getResourceAsStream('data_migration_alert').getText()
        List lines = dataRaw.split('\n')

        List exits = []
        List migrated = []

        lines.each {
            Alert alertExist = Alert.findByTermino(it)
            if (!alertExist) {
                Alert alertNew = new Alert(termino: it)
                alertNew.save()
                migrated << it
            } else {
                exits << it
            }
        }
        def map = [:]
        map.exist = exits
        map.migrated = migrated

        return map
    }

    def migrateCleanWatchlist() {
        def list = Bidding.findAllByObservada(true)
        list.each {
            it.cleanAllStrings()
        }

        def map = [:]
        map.migrated = list
        return map
    }

    private boolean onExeptionTakeAction(Exception ex, def param) {
        boolean takeAction = false
        if (ex instanceof DeadlockLoserDataAccessException
                || ex instanceof CannotAcquireLockException) {
            def message = "Handling " + ex.getClass().name
            println(message)
            myMailProviderService.reportEvent(message)
            takeAction = true
        } else {
            println(ex.getMessage())
            ex.printStackTrace()
            myMailProviderService.reportException(ex, param)
        }
        return takeAction
    }

}
