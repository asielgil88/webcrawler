package mail

import grails.gsp.PageRenderer
import grails.transaction.Transactional
import model.v2.Contact
import model.v2.bidding.Bidding
import org.codehaus.groovy.grails.web.mapping.LinkGenerator

import java.text.SimpleDateFormat

@Transactional
class MyMailProviderService {

    private static final String ADMIN_EMAIL = "asielgil88@gmail.com"
    final String COLOR_ALERT = "#FF9F00"
    final String COLOR_WATCH_ALERT = "#00b7c3"

    def mailService
    PageRenderer groovyPageRenderer
    LinkGenerator grailsLinkGenerator

    def insertAlert(Bidding bidding, String alert) {
        def contactList = Contact.getAll()

        for (Contact contactInfo : contactList) {
            def subject = "Nueva Licitación ${bidding.numero} (Alerta -> ${alert})"

            def title = "Alerta -> [${alert}]"
            def link = bidding.getPanamaLink()
            def message = "Nueva Licitación"

            sendEmail(contactInfo.email, subject, title, message, link, bidding, COLOR_ALERT)
        }
    }

    def watchAlert(Bidding bidding) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm a")
        def contactList = Contact.getAll()

        for (Contact contactInfo : contactList) {
            def subject = "Licitación Actualizada ${bidding.numero} -> ${format.format(new Date())}"

            def title = "Actualización Detectada -> ${format.format(new Date())}"
            def link = bidding.getPanamaLink()
            def message = "Licitación Actualizada"

            sendEmail(contactInfo.email, subject, title, message, link, bidding, COLOR_WATCH_ALERT)
        }
    }

    def watchAlertAdmin(Bidding bidding) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm a")
        def subject = "Licitación Actualizada ${bidding.numero} -> ${format.format(new Date())}"

        def title = "Actualización Detectada -> ${format.format(new Date())}"
        def link = bidding.getPanamaLink()
        def message = "Licitación Actualizada"

        sendEmail(ADMIN_EMAIL, subject, title, message, link, bidding, COLOR_WATCH_ALERT)
    }

    private void sendEmail(def recipient, def subjectStr, def title, def message, def link, def bidding, def headerColor) {
        def emailContent = groovyPageRenderer.render(view: "/mail_template/alert_templateV2",
                model: [title: title, message: message, link: link, entity: bidding, headerColor: headerColor])
        mailService.sendMail {
            async true
            from "infopanamacompra@gmail.com"
            to recipient
            subject subjectStr
            html emailContent
        }
    }

    def reportException(Exception ex, def param) {
        def emailContent = "Item: " + param + "\n\n" + ex.getMessage() + "\n" + ex.getLocalizedMessage() + "\n" + ex.getCause().toString()
        mailService.sendMail {
            async true
            from "infopanamacompra@gmail.com"
            to "asielgil88@gmail.com"
            subject "WebCrawlerV2 Exception"
            html emailContent
        }
    }

    def reportEvent(String event) {
        mailService.sendMail {
            async true
            from "infopanamacompra@gmail.com"
            to "asielgil88@gmail.com"
            subject "WebCrawlerV2 Event"
            html event
        }
    }

    def sendEmailTest(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm a")

        Bidding bidding = Bidding.first()
        def message = "Nueva Licitación"
        def alert = "PARACETAMOL"
        def title = "Actualización Detectada -> ${format.format(new Date())}"
        def link = bidding.getPanamaLink()
        def subjectStr = "Nueva Licitación ${bidding.numero} (FILTER - ${alert})" + new Date().toGMTString()

        sendEmail("asielgil88@gmail.com", subjectStr, title, message, link, bidding, COLOR_WATCH_ALERT)
    }

    @Deprecated
    private void sendReport(def recipient, def subjectStr, def title, def message) {
        def emailContent = groovyPageRenderer.render(view: "/mail_template/report_template",
                model: [title: title, message: message])
        mailService.sendMail {
            async true
            from "infopanamacompra@gmail.com"
            to recipient
            subject subjectStr
            html emailContent
        }
    }

    @Deprecated
    def watchListAlert(List<Bidding> biddings) {

        def contactList = Contact.getAll()

        for (Contact contactInfo : contactList) {
            def subject = "Licitaciones Actualizadas ${biddings.size()}"

            def title = "Dear ${contactInfo.name}, Actualizaciones"
            def message = "Nuestro sistema ha detectado actualizaciones en las licitaciones "
            if (biddings.size() > 0) {
                message += "<br /> <br /> <b> Actualizaciones: </b> <ul>"
                biddings.each {
                    message += "<li> <a href=\"${it.getPanamaLink()}\" target=\"_blank\">Panama Link</a>   ----- <a href=\"${grailsLinkGenerator.link(controller: 'bidding', action: 'show', id: it.id, absolute: true)}\" target=\"_blank\">${it.numero}</a>  </li>"
                }
                message += " </ul>"
            }

            sendReport(contactInfo.email, subject, title, message)
        }
    }

}
