<%@ page import="model.v2.bidding.Bidding" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="public">
    <g:set var="entityName" value="${message(code: 'bidding.label', default: 'Bidding')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div class="row">
    <div class="col-lg-12">
        <g:render template="bidding" model="[biddingInstance: biddingInstance, displayWatch: false]"/>
    </div>
    <!--/.col-->
</div>
<!--/.row-->
</body>
</html>
