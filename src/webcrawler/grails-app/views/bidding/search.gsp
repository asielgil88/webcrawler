<%@ page import="model.v2.bidding.Bidding" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
    <g:set var="entityName" value="${message(code: 'bidding.label', default: 'Bidding')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Busqueda</strong>
            </div>
            <div class="card-body">
                <g:form url='[controller: "bidding", action: "search"]' method="get" class="form-horizontal">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                    </span>
                                    <input value="${params.q}" type="text" id="input1-group2" name="q" class="form-control" placeholder="Type your search ...">
                            </div>
                        </div>
                    </div>
                </g:form>
                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Numero</th>
                        <th>Descripcion</th>
                        <th>Date Captured</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${biddingInstanceList}" var="entity">
                        <tr>
                            <td><a href="${createLink(controller: 'bidding', action: 'show', id: entity.id)}">${entity.numero}</a></td>
                            <td>${entity.descripcion.size() > 20 ? entity.descripcion.substring(0,20) + " ..." : entity.descripcion}</td>
                            <td><g:formatDate format="yyyy-MM-dd HH:mm a" date="${entity.dateCreated}"/></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
                <div class="pagination">
                    <g:paginate total="${totalCount ?: 0}" params="${[q:params.q]}"/>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
    <!--/.col-->
</div>
<!--/.row-->
</body>
</html>
