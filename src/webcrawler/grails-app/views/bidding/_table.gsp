<table class="table table-striped">
    <thead>
    <tr>
        <th>Numero</th>
        <th>Descripcion</th>
        <th>Date Captured</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${biddingInstanceList}" var="entity">
        <tr>
            <td><a href="${createLink(controller: 'bidding', action: 'show', id: entity.id)}">${entity.numero}</a></td>
            <td>${entity.descripcion.size() > 20 ? entity.descripcion.substring(0,20) + " ..." : entity.descripcion}</td>
            <td><g:formatDate format="yyyy-MM-dd HH:mm a" date="${entity.dateCreated}"/></td>
        </tr>
    </g:each>
    </tbody>
</table>