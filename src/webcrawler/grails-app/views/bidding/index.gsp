<%@ page import="model.v2.bidding.Bidding" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
    <title>Bidding</title>
</head>

<body>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Licitaciones
            </div>

            <div class="card-body">

                <g:render template="table"  model="[biddingInstanceList: biddingInstanceList]"/>

                <div class="pagination">
                    <g:paginate total="${totalCount ?: 0}" params="${[id:categoryID]}"/>
                </div>
            </div>
        </div>
    </div>
    <!--/.col-->
</div>
<!--/.row-->

</body>
</html>
