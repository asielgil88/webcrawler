<div class="card">
    <div class="card-header">
        <i class="fa fa-align-justify"></i> <b>Licitacion</b>
        <g:if test="${displayWatch}">

            <g:if test="${!biddingInstance.observada}">
                    <a href="${createLink(controller: 'bidding', action: 'toggleWatch', id: biddingInstance.id)}" class="btn btn-primary pull-right"><i
                            class="icon-eyeglass"></i> Add to WatchList </a>
                </g:if>
                <g:else>
                    <a href="${createLink(controller: 'bidding', action: 'toggleWatch', id: biddingInstance.id)}" class="btn btn-danger pull-right"><i
                            class="icon-eyeglass"></i> Remove from WatchList </a>

                </g:else>
        </g:if>
    </div>

    <div class="card-body">
        <div id="show-bidding" class="content scaffold-show" role="main">
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <ol class="property-list bidding">

                <g:if test="${biddingInstance?.numero}">
                    <li class="fieldcontain">
                        <span id="numero-label" class="property-label"><g:message code="bidding.numero.label"
                                                                                  default="Numero"/></span>

                        <span class="property-value" aria-labelledby="numero-label">
                            <a href="${biddingInstance.getPanamaLink()}">${biddingInstance.numero}</a>
                        </span>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.category}">
                    <li class="fieldcontain">
                        <span id="category-label" class="property-label"><g:message
                                code="bidding.category.label"
                                default="Category"/></span>

                        <span class="property-value" aria-labelledby="category-label">
                            ${biddingInstance?.category?.nivel1.encodeAsHTML()}
                        </span>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.contactUserCargo}">
                    <li class="fieldcontain">
                        <span id="contactUserCargo-label" class="property-label"><g:message
                                code="bidding.contactUserCargo.label" default="Contact User Cargo"/></span>

                        <span class="property-value" aria-labelledby="contactUserCargo-label"><g:fieldValue
                                bean="${biddingInstance}" field="contactUserCargo"/></span>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.contactUserNombre}">
                    <li class="fieldcontain">
                        <span id="contactUserNombre-label" class="property-label"><g:message
                                code="bidding.contactUserNombre.label" default="Contact User Nombre"/></span>

                        <span class="property-value" aria-labelledby="contactUserNombre-label"><g:fieldValue
                                bean="${biddingInstance}" field="contactUserNombre"/></span>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.dependencias}">
                    <li class="fieldcontain">
                        <span id="dependencias-label" class="property-label"><g:message
                                code="bidding.dependencias.label"
                                default="Dependencias"/></span>

                        <span class="property-value" aria-labelledby="dependencias-label"><g:fieldValue
                                bean="${biddingInstance}" field="dependencias"/></span>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.descripcion}">
                    <li class="fieldcontain">
                        <span id="descripcion-label" class="property-label"><g:message
                                code="bidding.descripcion.label"
                                default="Descripcion"/></span>

                        <span class="property-value" aria-labelledby="descripcion-label"><g:fieldValue
                                bean="${biddingInstance}"
                                field="descripcion"/></span>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.direccion}">
                    <li class="fieldcontain">
                        <span id="direccion-label" class="property-label"><g:message
                                code="bidding.direccion.label"
                                default="Direccion"/></span>

                        <span class="property-value" aria-labelledby="direccion-label"><g:fieldValue
                                bean="${biddingInstance}"
                                field="direccion"/></span>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.entidad}">
                    <li class="fieldcontain">
                        <span id="entidad-label" class="property-label"><g:message code="bidding.entidad.label"
                                                                                   default="Entidad"/></span>

                        <span class="property-value" aria-labelledby="entidad-label"><g:fieldValue
                                bean="${biddingInstance}"
                                field="entidad"/></span>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.objetoContratacion}">
                    <li class="fieldcontain">
                        <span id="objetoContratacion-label" class="property-label"><g:message
                                code="bidding.objetoContratacion.label" default="Objeto Contratacion"/></span>

                        <span class="property-value" aria-labelledby="objetoContratacion-label"><g:fieldValue
                                bean="${biddingInstance}" field="objetoContratacion"/></span>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.unidadDeCompra}">
                    <li class="fieldcontain">
                        <span id="unidadDeCompra-label" class="property-label"><g:message
                                code="bidding.unidadDeCompra.label"
                                default="Unidad De Compra"/></span>

                        <span class="property-value" aria-labelledby="unidadDeCompra-label"><g:fieldValue
                                bean="${biddingInstance}" field="unidadDeCompra"/></span>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.especificacionesTecnicas}">
                    <li class="fieldcontain">
                        <span id="especificacionesTecnicas-label" class="property-label"><g:message
                                code="bidding.especificacionesTecnicas.label"
                                default="Especificaciones Tecnicas"/></span>

                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>Descripcion</th>
                                <th>Cantidad</th>
                                <th>Glosa</th>
                            </tr>
                            </thead>
                            <tbody>

                            <g:each in="${biddingInstance.especificacionesTecnicas.getAllItems()}" var="d">
                                <tr>
                                    <td>${d.descripcion}</td>
                                    <td>${d.cantidad}</td>
                                    <td>${d.glosa}</td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>

                    </li>
                </g:if>

                <g:if test="${biddingInstance?.documentosAdjuntos}">
                    <li class="fieldcontain">
                        <span id="documentosAdjuntos-label" class="property-label"><g:message
                                code="bidding.documentosAdjuntos.label" default="Documentos Adjuntos"/></span>

                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>Comentario</th>
                                <th>Description</th>
                                <th>Fecha Creacion</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${biddingInstance.documentosAdjuntos}" var="d">
                                <tr>
                                    <td>${d.comentario}</td>
                                    <td>${d.description}</td>
                                    <td>${d.fechaCreacion}</td>
                                    <td>
                                        <a href="${util.UrlConstants.URL_BASE + d.link}">
                                            <i class="icon-cloud-download icons font-2xl d-block mt-4"></i>
                                        </a>
                                    </td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </li>
                </g:if>

                <g:if test="${biddingInstance?.avisos}">
                    <li class="fieldcontain">
                        <span id="avisos-label" class="property-label"><g:message code="bidding.avisos.label"
                                                                                  default="Avisos"/></span>

                        <table class="table table-striped table-sm">
                            <tbody>
                            <g:each in="${biddingInstance.avisos}" var="a">
                                <tr>
                                    <td>${a.label}</td>
                                    <td>${a.value_field}</td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>

                    </li>
                </g:if>

            </ol>
        </div>
    </div>
</div>