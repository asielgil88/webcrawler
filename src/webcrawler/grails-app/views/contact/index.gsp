<%@ page import="model.v2.Contact" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
    <title>Contactos</title>
</head>

<body>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Agregar Contacto
            </div>

            <div class="card-body">
                <g:form url='[controller: "contact", action: "create"]' method="get" >
                    <div class="input-group">
                            <input type="text" name="email" class="form-control" placeholder="Email">
                            <input type="text" name="name" class="form-control" placeholder="Person Name">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-send"></i>
                                    Submit
                                </button>
                            </span>
                    </g:form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Contactos
            </div>

            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Email</th>
                        <th>Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${contactInstanceList}" var="entity">
                        <tr>
                            <td>${entity.email}</td>
                            <td>${entity.name}</td>
                            <td>
                                <a href="${createLink(controller: 'contact', action: 'delete', id: entity.id)}" class="btn btn-danger">
                                    <i class="fa fa-remove"></i> Remove
                                </a>
                            </td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="pagination">
                    <g:paginate total="${totalCount ?: 0}"/>
                </div>
            </div>
        </div>
    </div>
    <!--/.col-->
</div>
<!--/.row-->

</body>
</html>
