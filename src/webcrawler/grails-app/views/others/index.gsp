<%@ page import="model.v2.Category" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
    <title>Otras Contrataciones Públicas</title>
</head>

<body>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Otras Contrataciones Públicas
            </div>

            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Total</th>
                        <th>Today</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${otherContractInstanceList}" var="entity">
                        <tr>
                            <td><a href="${createLink(controller: 'bidding', action: "getByOtherContract", id: entity.id)}" >${entity.name}</a></td>
                            <td>${entity.countBidTotal()}</td>
                            <td>${entity.countBidToday()}</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/.col-->
</div>
<!--/.row-->


<g:javascript>

    $(function () {

    });

</g:javascript>

</body>
</html>
