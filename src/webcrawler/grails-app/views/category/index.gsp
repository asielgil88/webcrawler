<%@ page import="model.v2.Category" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
    <title>Categories</title>
</head>

<body>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Oportunidades de negocio
                <sec:ifAllGranted roles="ROLE_ADMIN">
                <a href="${createLink(controller: 'category', action: 'categoryRefresh')}" type="button"
                   class="btn btn-outline-danger pull-right" style="margin-top: 1px;">
                    <i class="fa fa-refresh"></i>&nbsp; Refresh
                </a>
                </sec:ifAllGranted>
            </div>

            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Enabled</th>
                        <th>nivel1</th>
                        <th>PrimerNivel</th>
                        <th>Total</th>
                        <th>Today</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${categoryInstanceList}" var="entity">
                        <tr>
                            <td>
                                <label class="switch switch-3d switch-primary">
                                    <input type="checkbox" class="switch-input enable-cat"
                                           data-id="${entity.id}" ${entity.isEnabled() ? 'checked' : ''}>
                                    <span class="switch-label"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </td>
                            <td><a href="${createLink(controller: 'bidding', action: "index", id: entity.id)}" >${entity.nivel1}</a></td>
                            <td>${entity.primerNivel}</td>
                            <td>${entity.countBidTotal()}</td>
                            <td>${entity.countBidToday()}</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="pagination">
                    <g:paginate total="${totalCount ?: 0}"/>
                </div>
            </div>
        </div>
    </div>
    <!--/.col-->
</div>
<!--/.row-->


<g:javascript>
    $(function () {

        $('.switch-input').change(function () {

            var elem = $(this);
            var url = "";
            var idCat = elem.attr("data-id");
            if(elem.prop('checked')){
                url = "${createLink(controller: 'category', action: 'enableCategoria')}/";
            }else{
                url = "${createLink(controller: 'category', action: 'disableCategoria')}/";
            }



            $.ajax({
                url: url + idCat,
                headers: {
                    Accept: "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8"
                }
            }).done(function (data) {
                // check connection
                toastr.success(data.message);
            }).fail(function( data, textStatus, errorThrown ) {
                toastr.error(data.responseJSON.error);
                elem.prop('checked', !elem.prop('checked'));
            });
        });

    });

</g:javascript>

</body>
</html>
