<%@ page import="model.v2.Alert" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
    <title>Alertas</title>
</head>

<body>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Nueva Alerta
            </div>

            <div class="card-body">
                <g:form url='[controller: "alert", action: "create"]' method="get" >
                    <div class="input-group">
                            <input type="text" name="termino" class="form-control" placeholder="Type your new Alert">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-send"></i>
                                    Submit
                                </button>
                            </span>
                    </g:form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Alertas
            </div>

            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Termino de Alerta</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${alertInstanceList}" var="entity">
                        <tr>
                            <td>${entity.termino.size() > 20 ? entity.termino.substring(0, 20) + " ..." : entity.termino}</td>
                            <td>
                                <a href="${createLink(controller: 'alert', action: 'delete', id: entity.id)}" class="btn btn-danger">
                                    <i class="fa fa-remove"></i> Remove
                                </a>
                            </td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="pagination">
                    <g:paginate total="${totalCount ?: 0}"/>
                </div>
            </div>
        </div>
    </div>
    <!--/.col-->
</div>
<!--/.row-->

</body>
</html>
