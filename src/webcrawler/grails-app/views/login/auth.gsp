<!--
 * CoreUI - Open Source Bootstrap Admin Template
 * @version v1.0.0-alpha.6
 * @link http://coreui.io
 * Copyright (c) 2017 creativeLabs Łukasz Holeczek
 * @license MIT
 -->
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->

    <title>CoreUI Login</title>

    <!-- Icons -->
    <link href="${resource(dir: 'core_ui/css', file: 'font-awesome.min.css')}" rel="stylesheet">
    <link href="${resource(dir: 'core_ui/css', file: 'simple-line-icons.css')}" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="${resource(dir: 'core_ui/css', file: 'style.css')}" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group mb-0">
                <div class="card p-4">
                    <div class="card-body">
                        <form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>
                            <h1>Login</h1>

                            <p class="text-muted">Sign In to your account</p>
                            <g:if test='${flash.message}'>
                                <div class="alert alert-danger" role="alert">
                                    ${flash.message}
                                </div>
                            </g:if>
                            <div class="input-group mb-3">
                                <span class="input-group-addon"><i class="icon-user"></i>
                                </span>
                                <input type='text' class="form-control" placeholder="Username" name='j_username'
                                       id='username'/>
                            </div>

                            <div class="input-group mb-4">
                                <span class="input-group-addon"><i class="icon-lock"></i>
                                </span>
                                <input type='password' class="form-control" placeholder="Password" name='j_password'
                                       id='password'/>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary px-4">Login</button>
                                </div>

                                <div class="col-6 text-right">
                                    <input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me'
                                           <g:if test='${hasCookie}'>checked='checked'</g:if>/>
                                    <label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
                    <div class="card-body text-center">
                        <div>
                            <h2>Info</h2>

                            <p>Welcome!!! if you don't have enough access you make a Request.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap and necessary plugins -->
<script src="${resource(dir: 'core_ui/bower_components/jquery/dist', file: 'jquery.min.js')}"></script>
<script src="${resource(dir: 'core_ui/bower_components/bootstrap/dist/js', file: 'bootstrap.min.js')}"></script>

</body>

</html>