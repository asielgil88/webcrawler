<%--
  Created by IntelliJ IDEA.
  User: asiel.gil1
  Date: 8/20/2017
  Time: 1:03 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword"
          content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,Angular 2,Angular4,Angular 4,jQuery,CSS,HTML,RWD,Dashboard,React,React.js,Vue,Vue.js">
    <link rel="shortcut icon" href="${resource(dir: 'core_ui/img', file: 'favicon.png')}">
    <title>CoreUI - Open Source Bootstrap Admin Template</title>

    <!-- Icons -->
    <link href="${resource(dir: 'core_ui/css', file: 'font-awesome.min.css')}" rel="stylesheet">
    <link href="${resource(dir: 'core_ui/css', file: 'simple-line-icons.css')}" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="${resource(dir: 'core_ui/css', file: 'style.css')}" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
    .property-label{
        font-weight: bold;
    }
    </style>
    <g:layoutHead/>
    <r:layoutResources />

</head>

<!-- BODY options, add following classes to body to change options

// Header options
1. '.header-fixed'					- Fixed Header

// Sidebar options
1. '.sidebar-fixed'					- Fixed Sidebar
2. '.sidebar-hidden'				- Hidden Sidebar
3. '.sidebar-off-canvas'		- Off Canvas Sidebar
4. '.sidebar-minimized'			- Minimized Sidebar (Only icons)
5. '.sidebar-compact'			  - Compact Sidebar

// Aside options
1. '.aside-menu-fixed'			- Fixed Aside Menu
2. '.aside-menu-hidden'			- Hidden Aside Menu
3. '.aside-menu-off-canvas'	- Off Canvas Aside Menu

// Breadcrumb options
1. '.breadcrumb-fixed'			- Fixed Breadcrumb

// Footer options
1. '.footer-fixed'					- Fixed footer

-->

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
<header class="app-header navbar">
    <a class="navbar-brand" href="#"></a>

    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="#">System Panel</a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
    </ul>

</header>

<div class="app-body">

    <!-- Main content -->
    <main class="main" style="margin-top: 20px; margin-left: 0px!important;">

        <div class="container-fluid">

            <div class="animated fadeIn">
                <g:layoutBody/>
            </div>

        </div>
        <!-- /.conainer-fluid -->
    </main>

</div>

<footer class="app-footer" style="margin-left: 0px!important;">
    <a href="http://coreui.io">CoreUI</a> © 2017 creativeLabs.
    <span class="float-right">Powered by <a href="http://coreui.io">CoreUI</a>
    </span>
</footer>

<!-- Bootstrap and necessary plugins -->
<script src="${resource(dir: 'core_ui/bower_components/jquery/dist', file: 'jquery.min.js')}"></script>
<script src="${resource(dir: 'core_ui/bower_components/popper.js', file: 'index.js')}"></script>
<script src="${resource(dir: 'core_ui/bower_components/bootstrap/dist/js', file: 'bootstrap.min.js')}"></script>
<script src="${resource(dir: 'core_ui/bower_components/pace', file: 'pace.min.js')}"></script>


<!-- Plugins and scripts required by all views -->
<script src="${resource(dir: 'core_ui/bower_components/chart.js/dist', file: 'Chart.min.js')}"></script>

<!-- GenesisUI main scripts -->

<script src="${resource(dir: 'core_ui/js', file: 'app.js')}"></script>
<script src="${resource(dir: 'core_ui/js/views', file: 'main.js')}"></script>





<!-- Plugins and scripts required by this views -->

<!-- Custom scripts required by this view -->
%{--<script src="${resource(dir: 'core_ui/js/views', file: 'main.js')}"></script>--}%

<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<g:javascript>
            $(function(){
                    var error = '${flash.error}';
                    var message = '${flash.message}';
                    if(error){
                        toastr.error(error, 'Error!');
                    }
                    if(message){
                        toastr.info(message, 'Message!');
                    }

            })
</g:javascript>

<r:layoutResources />

</body>

</html>