<%--
  Created by IntelliJ IDEA.
  User: asiel.gil1
  Date: 8/20/2017
  Time: 1:03 AM
--%>

<%@ page import="grails.util.Holders" contentType="text/html;charset=UTF-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap ROLE_ADMIN Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword"
          content="Bootstrap,ROLE_ADMIN,Template,Open,Source,AngularJS,Angular,Angular2,Angular 2,Angular4,Angular 4,jQuery,CSS,HTML,RWD,Dashboard,React,React.js,Vue,Vue.js">
    <link rel="shortcut icon" href="${resource(dir: 'core_ui/img', file: 'favicon.png')}">
    <title>CoreUI - Open Source Bootstrap ROLE_ADMIN Template</title>

    <!-- Icons -->
    <link href="${resource(dir: 'core_ui/css', file: 'font-awesome.min.css')}" rel="stylesheet">
    <link href="${resource(dir: 'core_ui/css', file: 'simple-line-icons.css')}" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="${resource(dir: 'core_ui/css', file: 'style.css')}" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
    .property-label{
        font-weight: bold;
    }
    </style>

    <!-- Bootstrap and necessary plugins -->
    <script src="${resource(dir: 'core_ui/bower_components/jquery/dist', file: 'jquery.min.js')}"></script>
    <script src="${resource(dir: 'core_ui/bower_components/popper.js', file: 'index.js')}"></script>
    <script src="${resource(dir: 'core_ui/bower_components/bootstrap/dist/js', file: 'bootstrap.min.js')}"></script>
    <script src="${resource(dir: 'core_ui/bower_components/pace', file: 'pace.min.js')}"></script>


    <!-- Plugins and scripts required by all views -->
    <script src="${resource(dir: 'core_ui/bower_components/chart.js/dist', file: 'Chart.min.js')}"></script>

    <!-- GenesisUI main scripts -->

    <script src="${resource(dir: 'core_ui/js', file: 'app.js')}"></script>
    <script src="${resource(dir: 'core_ui/js/views', file: 'main.js')}"></script>
    <!-- Plugins and scripts required by this views -->

    <!-- Custom scripts required by this view -->
    %{--<script src="${resource(dir: 'core_ui/js/views', file: 'main.js')}"></script>--}%

    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <g:layoutHead/>
    <r:layoutResources />

</head>

<!-- BODY options, add following classes to body to change options

// Header options
1. '.header-fixed'					- Fixed Header

// Sidebar options
1. '.sidebar-fixed'					- Fixed Sidebar
2. '.sidebar-hidden'				- Hidden Sidebar
3. '.sidebar-off-canvas'		- Off Canvas Sidebar
4. '.sidebar-minimized'			- Minimized Sidebar (Only icons)
5. '.sidebar-compact'			  - Compact Sidebar

// Aside options
1. '.aside-menu-fixed'			- Fixed Aside Menu
2. '.aside-menu-hidden'			- Hidden Aside Menu
3. '.aside-menu-off-canvas'	- Off Canvas Aside Menu

// Breadcrumb options
1. '.breadcrumb-fixed'			- Fixed Breadcrumb

// Footer options
1. '.footer-fixed'					- Fixed footer

-->

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler sidebar-minimizer d-md-down-none" type="button">☰</button>

    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="#">System Panel</a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#"><i class="icon-location-pin"></i></a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button"
               aria-haspopup="true" aria-expanded="false">
                <img src="${resource(dir: 'core_ui/img/avatars', file: '7.jpg')}" class="img-avatar"
                     alt="ROLE_ADMIN@bootstrapmaster.com">
                <span class="d-md-down-none"><sec:loggedInUserInfo field="username"/></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>
                <a class="dropdown-item" href="${createLink(controller: 'logout')}"><i class="fa fa-lock"></i> Logout</a>
            </div>
        </li>
    </ul>

</header>

<div class="app-body">
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="${createLink(controller: 'category', action: 'index')}"><i
                            class="icon-layers"></i> Categorias</a>
                </li>
                <g:if test="${grails.util.Holders.config.grails.others.feature.enable}">
                    <li class="nav-item" >
                    <a class="nav-link" href="${createLink(controller: 'others', action: 'index')}"><i
                            class="icon-layers"></i> Otras Contrataciones</a>
                    </li>
                </g:if>
                <li class="nav-item">
                    <a class="nav-link" href="${createLink(controller: 'bidding', action: 'search')}"><i
                            class="fa fa-search"></i> Busqueda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${createLink(controller: 'bidding', action: 'watchlist')}"><i
                            class="icon-eyeglass"></i> Observadas</a>
                </li>
                <sec:ifAllGranted roles="ROLE_ADMIN">
                <li class="nav-item">
                    <a class="nav-link" href="${createLink(controller: 'alert', action: 'index')}"><i
                            class="icon-target"></i> Alertas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${createLink(controller: 'contact', action: 'index')}"><i
                            class="icon-people"></i> Contactos</a>
                </li>
                </sec:ifAllGranted>
            </ul>
        </nav>
    </div>

    <!-- Main content -->
    <main class="main">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item">ROLE_ADMIN</li>

            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu d-md-down-none">
                <div class="btn-group" role="group" aria-label="Button group">
                </div>
            </li>
        </ol>


        <div class="container-fluid">

            <div class="animated fadeIn">
                <g:layoutBody/>
            </div>

        </div>
        <!-- /.conainer-fluid -->
    </main>

</div>

<footer class="app-footer">
    <a href="http://coreui.io">CoreUI</a> © 2017 creativeLabs.
    <span class="float-right">Powered by <a href="http://coreui.io">CoreUI</a>
    </span>
</footer>

<g:javascript>
    $(function(){
        var error = '${flash.error}';
        var message = '${flash.message}';
        if(error){
            toastr.error(error, 'Error!');
        }
        if(message){
            toastr.info(message, 'Message!');
        }
    })
</g:javascript>

<r:layoutResources />

</body>

</html>