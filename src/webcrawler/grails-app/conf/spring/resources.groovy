import grails.util.Holders
// Place your Spring DSL code here
beans = {

    dataSource(com.mchange.v2.c3p0.ComboPooledDataSource) { bean ->
        bean.destroyMethod = 'close'
        //use grails' datasource configuration for connection user, password, driver and JDBC url
        user = Holders.config.dataSource.username
        password = Holders.config.dataSource.password
        driverClass = Holders.config.dataSource.driverClassName
        jdbcUrl = Holders.config.dataSource.url
        //force connections to renew after 2 hours
        maxConnectionAge = 600
        //get rid too many of idle connections after 30 minutes
        maxIdleTimeExcessConnections = 30 * 60

        maxPoolSize=30
        maxIdleTime=300
        idleConnectionTestPeriod = 1000
        maxStatementsPerConnection = 3
        autoCommitOnClose=true
        checkoutTimeout=10000
    }

}
