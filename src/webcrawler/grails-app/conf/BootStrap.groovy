import model.Role
import model.SysUser
import model.SysUserRole
import model.v2.OthersController
import model.v2.OtherContract

class BootStrap {

    def init = { servletContext ->

        if (SysUser.findAll().size() == 0) {
            def administrator = new SysUser()
            administrator.username = 'admin'
            administrator.password = 'lanco.2021!'
            administrator.save(flush: true)

            def adminAuthority = new Role()
            adminAuthority.authority = "ROLE_ADMIN"
            adminAuthority.save(flush: true)

            def userAuthority = new Role()
            userAuthority.authority = "ROLE_USER"
            userAuthority.save(flush: true)

            SysUserRole.create(administrator, adminAuthority, true)
        }

        if(OtherContract.findAll().size() == 0) {

            OtherContract o1 = new OtherContract()
            o1.name = "Cotizaciones en Línea"
            o1.jsonRquest = "{\"BusquedaTipos\":\"%s\",\"IdTipoBusqueda\":53,\"estado\":51,\"title\":\"Cotizaciones en Línea\",\"Inicio\":%s}"

            OtherContract o2 = new OtherContract()
            o2.name = "Mesa Conjunta MINSA-CSS"
            o2.jsonRquest = "{\"BusquedaTipos\":\"%s\",\"IdTipoBusqueda\":53,\"Inicio\":%s,\"entidad\":{\"nom\":\"Mesa Conjunta Minsa - Css\",\"id\":2023359,\"rut\":\"1--2-3\"},\"estado\":51,\"title\":\"Mesa Conjunta MINSA-CSS\",\"hf\":true}"

            OtherContract o3 = new OtherContract()
            o3.name = "Rendición de Cuentas COVID-19"
            o3.jsonRquest = "{\"BusquedaTipos\":\"%s\",\"IdTipoBusqueda\":51,\"title\":\"Rendición de Cuentas COVID-19\",\"Inicio\":%s}"

            o1.save(flush:true)
            o2.save(flush:true)
            o3.save(flush:true)
        }

    }
    def destroy = {
    }
}
